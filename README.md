# JCompleter #

A free extensible Java autocompletion library.

## Empower your entities with JCompleter! ##

1) Make a completable entity:

```
#!Java

public class Country implements Completable {
	private Integer id;
	private String name;
	private String currency;
	public String getCompletableText() {
		return name;
	}
}
```

2) Tell JCompleter how to load your completable entities:
```
#!Java

public class CountryLoader implements Loader {
	public Collection<? extends Completable> getElements() {
		return yourService.getCountries();
	}
}
public class CountryLoaderFactory implements LoaderFactory {
	public Loader create() {
		return new CountryLoader();
	}
}
```

3) Add jcompleter.xml config file in your class-path:


```
#!xml

<?xml version="1.0" encoding="UTF-8" ?>
<jcompleter>
	<completer name="countryCompleter" loaderFactory="com.example.CountryLoaderFactory" />
</jcompleter>

```


4) Now, your data is ready. Use a web-service out-of-the-box:

In your web.xml:

```
#!xml

<servlet>
	<description>JCompleter main servlet</description>
	<servlet-name>jcompleterServlet</servlet-name>
	<servlet-class>org.jcompleter.web.servlet.CompleterServlet</servlet-class>
</servlet>
<servlet-mapping>
	<servlet-name>jcompleterServlet</servlet-name>
	<url-pattern>/JCompleter</url-pattern>
</servlet-mapping>
```


And access it:

http://yourdomain.tld/JCompleter?completerName=countryCompleter
&query=e&responseType=json&numberOfResults=5&language=en

Result:

```
#!json


[
  {
    "id": 65,
    "name": "Estonia",
    "currency": "EUR",
    "completableId": "65",
    "completableText": "Estonia",
    "completableLocale": "en",
    "completableScore": 2
  },
  {
    "id": 43,
    "name": "Ecuador",
    "currency": "USD",
    "completableId": "43",
    "completableText": "Ecuador",
    "completableLocale": "en",
    "completableScore": 1
  },
  {
    "id": 28,
    "name": "Egypt",
    "currency": "EGP",
    "completableId": "28",
    "completableText": "Egypt",
    "completableLocale": "en",
    "completableScore": 0.7
  }
]
```

5) Or use it in your Java application directly:


```
#!Java

CompleterFacade completerFacade = new CompleterFacade();
AutocompleteRequest req = new AutocompleteRequest("Sy", Locale.ENGLISH);
AutocompleteResponse res = (AutocompleteResponse) completerFacade
.send("countryCompleter", req);
for(Completable c : res.getCompletables())
	System.out.println(c.getCompletableText());

```

## TODO ##
* Release first version
* Upload artifacts to Maven Central
* Add more integration examples