package org.jcompleter.example;

import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderFactory;

/**
 * A loader factory to allow creating countries {@link Loader}.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CountryLoaderFactory implements LoaderFactory {

	@Override
	public Loader create() {
		return new CountryLoader();
	}

}
