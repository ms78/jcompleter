package org.jcompleter.example;

import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderFactory;

/**
 * A loader factory to allow creating names {@link Loader}.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class NameLoaderFactory implements LoaderFactory {

	@Override
	public Loader create() {
		return new NameLoader();
	}

}
