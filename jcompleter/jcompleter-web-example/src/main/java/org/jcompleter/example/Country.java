package org.jcompleter.example;

import java.util.Locale;

import org.jcompleter.core.model.Completable;

/**
 * A simple country entity used as a demonstration of a Completable entity in
 * JCompleter.
 * 
 * @author Muthanna Alshoufi
 */
public class Country implements Completable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private String currency;
	private Locale locale;
	private Double score;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String getCompletableId() {
		return String.valueOf(id);
	}

	@Override
	public String getCompletableText() {
		return name;
	}

	@Override
	public Locale getCompletableLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	@Override
	public Double getCompletableScore() {
		return score;
	}

	public void settCompletableScore(Double score) {
		this.score = score;
	}

}
