package org.jcompleter.example;

import java.util.Locale;

import org.jcompleter.core.model.Completable;

/**
 * A simple name entity used as a demonstration for a Completable entity in
 * JCompleter.
 * 
 * @author Muthanna Alshoufi
 */
public class Name implements Completable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	protected Locale locale;
	protected double score;

	public Name(Integer id, String name, Locale locale) {
		this.id = id;
		this.name = name;
		this.locale = locale;
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	@Override
	public String getCompletableId() {
		return this.id + "";
	}

	@Override
	public String getCompletableText() {
		return name;
	}

	@Override
	public Locale getCompletableLocale() {
		return locale;
	}

	@Override
	public Double getCompletableScore() {
		return 0d;
	}

	@Override
	public String toString() {
		return "Name [id=" + id + ", name=" + name + ", locale=" + locale + ", score=" + score + "]";
	}

}
