package org.jcompleter.example;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.load.Loader;

/**
 * A simple country list used for auto-completion test.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CountryLoader implements Loader {

	@Override
	public Collection<? extends Completable> getElements() {
		Collection<Completable> list = new ArrayList<Completable>();

		Locale[] locales = { Locale.ENGLISH, Locale.FRENCH };

		for (Locale locale : locales) {
			list.addAll(getAllCountries(locale));
		}

		return list;
	}

	private Collection<Completable> getAllCountries(Locale locale) {
		Collection<Completable> countries = new ArrayList<Completable>();
		int count = 0;
		for (Locale loopLocale : Locale.getAvailableLocales()) {
			String name = loopLocale.getDisplayCountry(locale);

			if (name != null && !"".equals(name)) {
				Country country = new Country();
				country.setName(name);
				country.setId(++count);
				country.setCurrency("CAD");
				country.setLocale(locale);
				double score = 0d;
				if (name == "Ecuador")
					score = 1;
				if (name == "Estonia")
					score = 2;
				if (name == "Egypt")
					score = 0.7;
				country.settCompletableScore(score);

				countries.add(country);
			}
		}

		return countries;
	}

}
