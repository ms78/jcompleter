package org.jcompleter.example;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.load.Loader;

/**
 * A simple name list used for auto-completion test.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class NameLoader implements Loader {

	@Override
	public Collection<? extends Completable> getElements() {
		Collection<Completable> list = new ArrayList<Completable>();
		Locale[] locales = { new Locale("en"), new Locale("ar"), new Locale("fr", "FR"), new Locale("en", "CA") };
		for (Locale locale : locales) {
			try {
				String data = "";
				String fileName = "/names_" + locale.toString() + ".txt";
				InputStream stream = NameLoader.class.getResourceAsStream(fileName);
				BufferedReader in = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
				int count = 0;
				Collection<Completable> names = new ArrayList<Completable>();
				while (data != null) {
					data = in.readLine();
					if (data != null && !data.isEmpty())
						names.add(new Name(++count, data, locale));
				}

				list.addAll(names);

				in.close();
			} catch (Exception e) {
			}

		}

		return list;
	}

}
