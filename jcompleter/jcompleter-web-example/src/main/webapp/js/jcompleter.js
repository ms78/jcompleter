
function Jcompleter(options) {
	
	this.options = options;
	this.serviceUrl = ((options.serviceBaseUrl === undefined) ? '/JCompleter' : options.serviceBaseUrl)
									 + '?completerName=' + options.completerName 
									 + '&callback=' + (options.callback === undefined ? 'populateData' : options.callback)
									 + '&responseType=' + (options.responseType === undefined ? '' : options.responseType)
	 								 + '&numberOfResults=' + (options.numberOfResults === undefined ? '' : options.numberOfResults)
									 + '&language=' + (options.language === undefined ? '' : options.language)
									 + '&sorterType=' + (options.sorterType === undefined ? '' : options.sorterType);
	
	this.inputRef = document.getElementById(options.inputId);

	this.autocomplete = function() {
		if(this.inputRef.value)
			this.callAjax(this.serviceUrl + '&query=' + this.inputRef.value);
	};
	
	/** Calls Ajax using the passed url */ 
	this.callAjax = function(serviceUrl) {
	   var src = serviceUrl + '&time=' + new Date().getTime();
	   
	   var head = document.getElementsByTagName("head").item(0);
	   var script = document.getElementById("ajaxScriptTag");
	   if (script) {
	       head.removeChild(script);
	   }
	   
	   /** Create the script tag and add it to the head */
	   script = document.createElement("script");
	   script.setAttribute("id", "ajaxScriptTag");
	   script.setAttribute("type", "text/javascript");
	   script.setAttribute("src", src);
	   head.appendChild(script);
	};
	
	/** Populates the response */
	populateData = function(suggestions) {
	   if (suggestions) {
		   var newChild = '<ul>';		   
	       for ( var i in suggestions) {
    			newChild += '<li><strong>' + suggestions[i].completableText + '</strong> with entry id=' + suggestions[i].completableId 
    			+ ' score' + suggestions[i].completableScore + '</li>';
	       }
	       newChild += '</ul>';
	       
	       document.getElementById('jcompleterDiv1').innerHTML = newChild;
	       
	       if(suggestions.length > 0) {
	    	   document.getElementById('jcompleterDiv1').style.display = 'block';
	       } else {
	    	   document.getElementById('jcompleterDiv1').style.display = 'none';
	       }
	       
	   } else {
	   }
	}
}