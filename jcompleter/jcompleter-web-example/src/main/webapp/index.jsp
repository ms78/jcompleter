<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
 <link href="css/jcompleter.css" rel="stylesheet" type="text/css" />
 <script src="js/jcompleter.js" type="text/javascript"></script>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>

<br />
<br />
<br />
<br />
<form method="POST" action="/">
<table>

	<tr>
		<td>Country:</td>
		<td><input id="countryInput" name="countryInput" size="45" onkeyup="countryCompleter.autocomplete();" autofocus /></td>
	</tr>
	
	<tr>
		<td>Person:</td>
		<td><input id="personInput" name="personInput" size="45" onkeyup="personCompleter.autocomplete();" /></td>
	</tr>
	
</table>
</form>

<div id="jcompleterDiv" style="position: absolute; z-index: 9999;">
	<div class="autocomplete-w1">
		<div class="autocomplete" id="jcompleterDiv1"
			style="display: none;"></div>
	</div>
</div>


<script type="text/javascript">

var countryCompleter = new Jcompleter({
		'inputId' : 'countryInput',
		 'completerName' : 'countryCompleter', 
		 'language' : '',
		 'numberOfResults' : 5,
		 'sorterType' : 'SCORE'
		 });
		 
var personCompleter = new Jcompleter({
	'inputId' : 'personInput',
	'completerName' : 'personCompleter',
	'language' : 'ar',
	'numberOfResults' : 10,
	});
	
</script>
</body>
</html>

