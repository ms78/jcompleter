package org.jcompleter.core;

import org.jcompleter.core.cache.CacheTest;
import org.jcompleter.core.model.CompleterTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Launches all unit tests for JCompleter.
 * 
 * @author Muthanna Alshoufi
 *
 */
@RunWith(Suite.class)
// classes included in the test suite
@SuiteClasses({ FacadeTest.class, CompleterTest.class, CacheTest.class })
public class AllTests {

	@BeforeClass
	public static void setUpBefore() throws Exception {
	}

	@AfterClass
	public static void tearDownAfter() throws Exception {
	}

}
