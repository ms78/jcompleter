package org.jcompleter.core.model;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.jcompleter.core.exception.CompleterConfigurationException;
import org.jcompleter.core.exception.CompleterException;
import org.jcompleter.core.model.api.request.AutocompleteRequest;
import org.jcompleter.core.model.api.request.Request;
import org.jcompleter.core.model.api.response.AutocompleteResponse;
import org.jcompleter.core.model.api.response.Response;
import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test case for basic completer functionalities.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class CompleterTest {

	private static Completer<Element> COMPLETER;
	private static Collection<Completable> COLLECTION;

	private static Completable build(final String text, final String id, final Locale locale) {
		return new Completable() {

			private static final long serialVersionUID = 1L;

			@Override
			public String getCompletableText() {
				return text;
			}

			@Override
			public Double getCompletableScore() {
				return 0d;
			}

			@Override
			public Locale getCompletableLocale() {
				return locale;
			}

			@Override
			public String getCompletableId() {
				return id;
			}

		};
	}

	@BeforeClass
	public static void setUp() throws CompleterConfigurationException {

		COLLECTION = new ArrayList<Completable>();
		Completable country = build("Syrian", "Syrian Id", Locale.ENGLISH);
		COLLECTION.add(country);

		country = build("Great Britain", "Great Britain Id", Locale.ENGLISH);
		COLLECTION.add(country);

		country = build("zimbabwe", "zimbabwe Id", Locale.ENGLISH);
		COLLECTION.add(country);

		country = build("Canada", "Canada Id", Locale.ENGLISH);
		COLLECTION.add(country);

		String ountry = "ountry";
		char prefix = 'a';
		for (int j = 0; j < 26; j++) {
			for (int i = 0; i < 100; i++) {
				String s = prefix + ountry + i;
				country = build(s, s, Locale.ENGLISH);
				COLLECTION.add(country);
			}

			prefix++;
		}

		final Loader loader = new Loader() {
			@Override
			public Collection<? extends Completable> getElements() {
				return COLLECTION;
			}
		};

		LoaderFactory loaderFactory = new LoaderFactory() {

			@Override
			public Loader create() {
				return loader;
			}
		};

		try {
			COMPLETER = new Completer<Element>().setName("Test").setLoaderFactory(loaderFactory).build();
		} catch (CompleterException e) {
			e.printStackTrace();
		}

	}

	@AfterClass
	public static void tearDown() {
		COMPLETER.destroy();
	}

	@Test
	public void testCreate() {
		assertNotNull(COMPLETER);
	}

	@Test
	public void testAutocomplete() throws CompleterException {
		Request req;
		Response res;

		req = new AutocompleteRequest("a", Locale.ENGLISH, 10);
		long now = System.currentTimeMillis();
		// result = completer.autocompleteReturnSet(completerRequest);
		res = COMPLETER.process(req);
		now = System.currentTimeMillis() - now;
		System.out.println("Serving " + req + " took " + now + " ms");

		req = new AutocompleteRequest("a", Locale.ENGLISH, 10);
		now = System.currentTimeMillis();
		// result = completer.autocompleteReturnSet(completerRequest);
		res = COMPLETER.process(req);
		now = System.currentTimeMillis() - now;
		System.out.println("Serving " + req + " from cache took " + now + " ms");

		req = new AutocompleteRequest("z", Locale.ENGLISH, 10);
		now = System.currentTimeMillis();
		res = COMPLETER.process(req);
		now = System.currentTimeMillis() - now;
		System.out.println("Serving " + req + " took " + now + " ms");

		req = new AutocompleteRequest("z", Locale.ENGLISH, 10);
		now = System.currentTimeMillis();
		res = COMPLETER.process(req);
		now = System.currentTimeMillis() - now;
		System.out.println("Serving res " + res + " for req " + req + " from cache took " + now + " ms");
	}

	@Test
	public void testThreads() throws InterruptedException {
		List<Thread> listOfThreads = new ArrayList<Thread>();

		for (int i = 1; i < 10; i++) {

			Thread thread = new Thread("toto" + i) {

				@Override
				public void run() {
					char c = 'a';
					for (int i = 0; i < 10; i++) {
						Request completerRequest = new AutocompleteRequest(c + "", Locale.ENGLISH, 10);
						long now = System.currentTimeMillis();
						AutocompleteResponse response = null;
						try {
							response = (AutocompleteResponse) COMPLETER.process(completerRequest);
						} catch (CompleterException e) {
							e.printStackTrace();
						}
						System.out.println("Thread: " + getName() + " char: " + c + " Response time: "
								+ (System.currentTimeMillis() - now));
						for (Completable e : response.getCompletables())
							System.out.println(e.getCompletableText());
						c++;
						if (c == 123)
							c = 'a';
						try {
							sleep(100);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
				}
			};
			thread.start();
			listOfThreads.add(thread);
		}

		for (Thread thread : listOfThreads) {
			thread.run();
			thread.join();
		}
	}

}