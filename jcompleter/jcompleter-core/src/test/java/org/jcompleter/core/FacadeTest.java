package org.jcompleter.core;

import java.util.Locale;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.Completer;
import org.jcompleter.core.model.Element;
import org.jcompleter.core.model.api.response.AutocompleteResponse;
import org.jcompleter.core.model.load.CountryDataLoader;
import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderFactory;
import org.jcompleter.core.model.sort.SorterType;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test case for {@link CompleterFacade}.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class FacadeTest {

	@BeforeClass
	public static void setUp() {
	}

	@Test
	public void testFacadeFromConfig() throws Exception {

		CompleterFacade completerFacade = new CompleterFacade();

		AutocompleteResponse response = completerFacade.autocomplete("completer1", "s", Locale.ENGLISH, 5,
				SorterType.LEXICOGRAPHIC);

		for (Element element : response.getElements()) {
			System.out.println(element.getValue().getCompletableText());
		}

		System.out.println("\n------------------------------\n");

		response = completerFacade.autocomplete("completer2", "f", Locale.FRENCH, 5,
				SorterType.LEXICOGRAPHIC);
		for (Element element : response.getElements()) {
			System.out.println(element.getValue().getCompletableText());
		}

		for (Completable c : response.getCompletables()) {
			System.out.println(c.getCompletableText());
		}

		completerFacade.close();
	}

	@Test
	public void testFacadeProgrammatically() throws Exception {

		LoaderFactory loaderFactory = new LoaderFactory() {

			@Override
			public Loader create() {
				return new CountryDataLoader();
			}
		};

		Completer<Element> completer = new Completer<Element>().setName("countries").setLoaderFactory(loaderFactory)
				.build();

		CompleterFacade facade = new CompleterFacade();
		facade.register(completer);

		AutocompleteResponse res = (AutocompleteResponse) facade.autocomplete("countries", "ecu", Locale.FRENCH, 5,
				SorterType.LEXICOGRAPHIC);
		for (Completable c : res.getCompletables())
			System.out.println(c.getCompletableText());
		facade.close();
	}

}
