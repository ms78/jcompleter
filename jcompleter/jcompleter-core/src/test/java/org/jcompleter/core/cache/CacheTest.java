package org.jcompleter.core.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * LRU Used Cache test.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class CacheTest {

	protected static int CACHE_SIZE = 5;
	protected static Cache<Integer, String> CACHE;

	@BeforeClass
	public static void setUp() {
		CacheFactory<Integer, String> cacheFactory = new DefaultCacheFactory<Integer, String>();
		CACHE = cacheFactory.create(CACHE_SIZE);
	}

	@AfterClass
	public static void tearDown() {
		CACHE = null;
	}

	@Test
	public void testCreation() {
		// creation
		assertNotNull(CACHE);
		assertTrue(CACHE.isEmpty());
	}

	@Test
	public void testGetPutRemove() {

		// put, get and getNumberOfElementsInCache
		CACHE.put(1, "1");
		assertEquals(CACHE.getSize(), 1);
		assertFalse(CACHE.containsKey(2));
		assertEquals(CACHE.get(1), "1");
		CACHE.put(2, "2");
		assertEquals(CACHE.getSize(), 2);
		assertEquals(CACHE.get(2), "2");

		// change the value of 1
		CACHE.put(1, "2");
		assertEquals(CACHE.getSize(), 2);
		assertEquals(CACHE.get(1), "2");
		// remove 1
		CACHE.remove(1);
		assertNull(CACHE.get(1));
		assertEquals(CACHE.getSize(), 1);

		CACHE.flush();
	}

	@Test
	public void testClear() {
		CACHE.put(1, "Hello 1");
		CACHE.flush();
		assertNull(CACHE.get(1));
		assertEquals(CACHE.getSize(), 0);
	}

	@Test
	public void testGetAll() {
		CACHE.put(7, "7");
		CACHE.put(8, "8");
		Map<Integer, String> allEntries = CACHE.getAll();
		assertNotNull(allEntries);
		assertEquals(allEntries.size(), 2);
		for (Map.Entry<Integer, String> entry : allEntries.entrySet()) {
			assertNotNull(entry);
		}

		CACHE.flush();
	}

	@Test
	public void testLRUFunctionality() {

		for (int i = 1; i <= CACHE_SIZE; i++) {
			CACHE.put(new Integer(i), i + "");
		}

		assertEquals(CACHE.getSize(), CACHE_SIZE);

		// All elements get accessed but the last one
		for (int i = 1; i <= CACHE_SIZE - 1; i++) {
			CACHE.get(i);
		}

		// Add a new entry to make the number of entries in cache greater than
		// the cache max capacity
		CACHE.put(CACHE_SIZE + 1, (CACHE_SIZE + 1) + "");

		// the entry which hasn't been accessed should have already disappeared
		assertNull(CACHE.get(CACHE_SIZE));
		assertEquals(CACHE.getSize(), CACHE_SIZE);
		assertEquals(CACHE.get(CACHE_SIZE + 1), (CACHE_SIZE + 1) + "");

		for (int i = 1; i <= CACHE_SIZE - 1; i++) {
			assertEquals(CACHE.get(i), i + "");
		}

		CACHE.put(CACHE_SIZE + 2, (CACHE_SIZE + 2) + "");
		// bye bye element at (cacheSize+1)
		assertEquals(CACHE.get(CACHE_SIZE + 1), null);
		assertEquals(CACHE.getSize(), CACHE_SIZE);

		// access the one we've already added
		CACHE.get(CACHE_SIZE + 2);

		// Add two new entries
		CACHE.put(CACHE_SIZE + 3, (CACHE_SIZE + 3) + "");
		CACHE.put(CACHE_SIZE + 4, (CACHE_SIZE + 4) + "");

		// elements 1 & 2 should have already disappeared
		assertEquals(CACHE.get(1), null);
		assertEquals(CACHE.get(2), null);

		CACHE.flush();
	}

}
