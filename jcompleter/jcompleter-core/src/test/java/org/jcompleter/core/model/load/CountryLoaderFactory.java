package org.jcompleter.core.model.load;

/**
 * A loader factory to allow creating countries {@link Loader}.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CountryLoaderFactory implements LoaderFactory {

	@Override
	public Loader create() {
		return new CountryDataLoader();
	}

}
