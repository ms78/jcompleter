package org.jcompleter.core.model.load;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Locale;

import org.jcompleter.core.model.Completable;

/**
 * A simple country list used for auto-completion test.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CountryDataLoader implements Loader {

	@Override
	public Collection<? extends Completable> getElements() {
		Collection<Completable> list = new ArrayList<Completable>();

		Locale[] locales = { Locale.ENGLISH, Locale.FRENCH };
		for (Locale locale : locales) {
			list.addAll(getAllCountries(locale));
		}

		return list;
	}

	private Collection<Completable> getAllCountries(final Locale locale) {
		Collection<Completable> countries = new ArrayList<Completable>();
		int count = 0;
		for (Locale loopLocale : Locale.getAvailableLocales()) {
			final String name = loopLocale.getDisplayCountry(locale);
			final int finalCount = ++count;
			if (name != null && !"".equals(name)) {
				Completable country = new Completable() {

					private static final long serialVersionUID = 1L;

					@Override
					public String getCompletableText() {
						return name;
					}

					@Override
					public Double getCompletableScore() {
						return 0d;
					}

					@Override
					public Locale getCompletableLocale() {
						return locale;
					}

					@Override
					public String getCompletableId() {
						return finalCount + "";
					}

				};

				countries.add(country);
			}
		}

		return countries;
	}

}
