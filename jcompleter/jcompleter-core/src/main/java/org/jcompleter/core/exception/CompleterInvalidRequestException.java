package org.jcompleter.core.exception;

/**
 * Exception thrown by JCompleter when the request is invalid or missing
 * mandatory attributes.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterInvalidRequestException extends CompleterException {

	private static final long serialVersionUID = 1L;

	public CompleterInvalidRequestException(String s) {
		super(s);
	}

	public CompleterInvalidRequestException(Throwable e) {
		super(e);
	}

	public CompleterInvalidRequestException(String s, Throwable e) {
		super(s, e);
	}
}
