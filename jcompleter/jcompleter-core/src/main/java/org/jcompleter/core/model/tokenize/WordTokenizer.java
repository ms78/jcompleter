package org.jcompleter.core.model.tokenize;

import java.text.BreakIterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jcompleter.core.util.CommonUtil;

/**
 * Tokenizes Strings into words.
 * 
 * @author Muthanna Alshoufi
 * 
 */
class WordTokenizer implements Tokenizer {

	private Locale locale;

	WordTokenizer(Locale locale) {
		this.locale = locale;
	}

	@Override
	public Map<String, List<String>> tokenize(String input) {
		List<String> listOfWords = new LinkedList<String>();
		Map<String, List<String>> result = new LinkedHashMap<String, List<String>>();

		if (!CommonUtil.isBlank(input)) {
			BreakIterator breakIterator = BreakIterator.getWordInstance(locale);
			breakIterator.setText(input);
			int start = breakIterator.first();
			int end = breakIterator.next();
			while (end != BreakIterator.DONE) {
				String word = input.substring(start, end);
				listOfWords.add(word);
				start = end;
				end = breakIterator.next();
			}

			result.put(input, listOfWords);
		}

		return result;
	}

}
