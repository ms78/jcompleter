package org.jcompleter.core.model.normalize;

import java.text.Normalizer;
import java.util.regex.Pattern;

/**
 * Normalizes the String accented characters.
 * 
 * @author Muthanna Alshoufi
 *
 */
class DiacriticsNormalizer implements org.jcompleter.core.model.normalize.Normalizer {

    private static Pattern DIACRITICS_PATTERN = Pattern.compile("\\p{M}+");

	DiacriticsNormalizer() {
	}

	@Override
	public String normalize(String input) {
		if (input == null)
			return null;

		String decomposed = Normalizer.normalize(input, Normalizer.Form.NFD);
	    return DIACRITICS_PATTERN.matcher(decomposed).replaceAll("");
	}
	
}
