package org.jcompleter.core.conf;

import java.io.Serializable;

/**
 * A representation of the completer XML configuration entry in JCompleter
 * config file jcompleter.xml that is parsed by {@link ConfigManager}.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class ConfigEntry implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;
	private Integer numberOfQueriesToCache;
	private Integer minimumQueryLengthToCache;
	private Integer maximumQueryLengthToCache;

	private String loaderFactory;
	private String tokenizerFactory;
	private String indexerFactory;
	private String sorterFactory;
	private String normalizerFactory;

	ConfigEntry() {
	}

	public String getName() {
		return name;
	}

	public ConfigEntry setName(String name) {
		this.name = name;
		return this;
	}

	public Boolean isCachingEnabled() {
		return numberOfQueriesToCache != null && numberOfQueriesToCache > 0;
	}

	public Integer getNumberOfQueriesToCache() {
		return numberOfQueriesToCache;
	}

	public ConfigEntry setNumberOfQueriesToCache(Integer numberOfQueriesToCache) {
		this.numberOfQueriesToCache = numberOfQueriesToCache;
		return this;
	}

	public Integer getMinimumQueryLengthToCache() {
		return minimumQueryLengthToCache;
	}

	public ConfigEntry setMinimumQueryLengthToCache(Integer minimumQueryLengthToCache) {
		this.minimumQueryLengthToCache = minimumQueryLengthToCache;
		return this;
	}

	public Integer getMaximumQueryLengthToCache() {
		return maximumQueryLengthToCache;
	}

	public ConfigEntry setMaximumQueryLengthToCache(Integer maximumQueryLengthToCache) {
		this.maximumQueryLengthToCache = maximumQueryLengthToCache;
		return this;
	}

	public String getLoaderFactory() {
		return loaderFactory;
	}

	public ConfigEntry setLoaderFactory(String loaderFactory) {
		this.loaderFactory = loaderFactory;
		return this;
	}

	public String getTokenizerFactory() {
		return tokenizerFactory;
	}

	public ConfigEntry setTokenizerFactory(String tokenizerFactory) {
		this.tokenizerFactory = tokenizerFactory;
		return this;
	}

	public String getIndexerFactory() {
		return indexerFactory;
	}

	public ConfigEntry setIndexerFactory(String indexerFactory) {
		this.indexerFactory = indexerFactory;
		return this;
	}

	public String getSorterFactory() {
		return sorterFactory;
	}

	public ConfigEntry setSorterFactory(String sorterFactory) {
		this.sorterFactory = sorterFactory;
		return this;
	}

	public String getNormalizerFactory() {
		return normalizerFactory;
	}

	public ConfigEntry setNormalizerFactory(String normalizerFactory) {
		this.normalizerFactory = normalizerFactory;
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConfigEntry other = (ConfigEntry) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ConfigEntry [name=" + name + ", loaderFactory=" + loaderFactory + ", numberOfQueriesToCache="
				+ numberOfQueriesToCache + ", minimumQueryLengthToCache=" + minimumQueryLengthToCache
				+ ", maximumQueryLengthToCache=" + maximumQueryLengthToCache + ", tokenizerFactory=" + tokenizerFactory
				+ ", indexerFactory=" + indexerFactory + ", sorterFactory=" + sorterFactory + ", normalizerFactory="
				+ normalizerFactory + "]";
	}

}
