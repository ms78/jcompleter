package org.jcompleter.core.model.index;

/**
 * Creates {@link NavigableMapIndexer} implementations of {@link Indexer}
 * interface.
 * 
 * @author Muthanna Alshoufi
 */
public class DefaultIndexerFactory<E> implements IndexerFactory<E> {

	/**
	 * Creates a {@link NavigableMapIndexer} implementation of {@link Indexer}.
	 */
	@Override
	public Indexer<E> create() {
		return new NavigableMapIndexer<E>();
	}

}
