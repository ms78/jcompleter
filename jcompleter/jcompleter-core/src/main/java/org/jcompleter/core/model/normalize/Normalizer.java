package org.jcompleter.core.model.normalize;

/**
 * Normalizes Strings. The implementation will determine the type of
 * normalization to be applied on the input String.
 * 
 * @author Muthanna Alshoufi
 *
 */
public interface Normalizer {

	String normalize(String input);
}
