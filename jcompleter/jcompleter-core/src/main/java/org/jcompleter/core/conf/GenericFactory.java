package org.jcompleter.core.conf;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A generic factory that creates an instance from a class name dynamically.
 * Useful when we have the name of the class set in jcompleter.xml config file.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class GenericFactory<T> {

	private static final Logger logger = Logger.getLogger(GenericFactory.class.getName());

	public GenericFactory() {
	}

	/**
	 * Creates an object of type T from the class full name.
	 * 
	 * @param className
	 *            the full class name
	 * @param implClass
	 *            the expected implementation {@link Class}
	 * @return an instance of type T
	 */
	@SuppressWarnings("unchecked")
	public T create(String className, Class<?> implClass) {
		T impl = null;
		if (className != null) {
			try {
				Class<?> clazz = Class.forName(className);
				Object o = clazz.newInstance();
				if (implClass.isAssignableFrom(o.getClass()))
					impl = (T) o;
			} catch (InstantiationException ie) {
				logger.log(Level.SEVERE, className + " can not be instantiated.\n", ie);
			} catch (IllegalAccessException iae) {
				logger.log(Level.SEVERE, className + " must have a no-arg constructor.\n", iae);
			} catch (ClassNotFoundException cnfe) {
				logger.log(Level.SEVERE, className + " must be in class path.\n", cnfe);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Something went wrong while instantiating " + className + ".\n", e);
			}
		}

		return impl;
	}

}
