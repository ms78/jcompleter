package org.jcompleter.core.cache;

/**
 * Creates {@link LRUCache} implementations of {@link Cache} interface.
 * 
 * @author Muthanna Alshoufi
 */
public class DefaultCacheFactory<K, V> implements CacheFactory<K, V> {

	@Override
	public Cache<K, V> create(int cacheSize) {
		return new LRUCache<K, V>(cacheSize);
	}

}
