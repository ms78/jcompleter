package org.jcompleter.core.model.api;

import java.io.Serializable;
import java.util.Date;

/**
 * Parent Abstract class of all JCompleter communication message
 * implementations.
 * 
 * @author Muthanna Alshoufi
 *
 */
public abstract class ApiMessage implements Serializable {

	private static final long serialVersionUID = 1L;

	protected final Date date;

	public ApiMessage() {
		this.date = new Date();
	}

	/**
	 * Gets the request date.
	 * 
	 * @return a {@link Date} object
	 */
	public Date getDate() {
		return date;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApiMessage other = (ApiMessage) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}

}
