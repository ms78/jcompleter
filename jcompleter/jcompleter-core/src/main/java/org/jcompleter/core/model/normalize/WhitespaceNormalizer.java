package org.jcompleter.core.model.normalize;

/**
 * An implementation of {@link Normalizer} to remove extra spaces.
 * 
 * @author Muthanna Alshoufi
 */
class WhitespaceNormalizer implements Normalizer {

	WhitespaceNormalizer() {
	}

	@Override
	public String normalize(String input) {
		if (input != null)
			return input.replaceAll("\\s+", " ").trim();

		return null;
	}

}
