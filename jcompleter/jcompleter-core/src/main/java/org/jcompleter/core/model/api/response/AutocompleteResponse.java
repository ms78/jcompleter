package org.jcompleter.core.model.api.response;

import java.util.LinkedList;
import java.util.List;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.Element;
import org.jcompleter.core.model.api.request.Request;

/**
 * The main autocompleter implementation of {@link Response}.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class AutocompleteResponse extends Response {

	private static final long serialVersionUID = 1L;

	private final List<? extends Element> elements;

	public AutocompleteResponse(Request request, List<? extends Element> elements) {
		super(request);
		this.elements = elements;
	}

	/**
	 * Gets the list of {@link Element} instances.
	 * 
	 * @return a list of {@link Element} instances.
	 */
	public List<? extends Element> getElements() {
		return elements;
	}

	/**
	 * Gets the list of {@link Completable} instances.
	 * 
	 * @return a list of {@link Completable} instances
	 */
	public List<Completable> getCompletables() {
		List<Completable> list = new LinkedList<Completable>();
		for (Element e : elements) {
			list.add(e.getValue());
		}

		return list;
	}

	/**
	 * Gets the number of results contained in the response.
	 * 
	 * @return an integer representing the number of results
	 */
	public int getSize() {
		return (elements != null ? elements.size() : 0);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((elements == null) ? 0 : elements.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutocompleteResponse other = (AutocompleteResponse) obj;
		if (elements == null) {
			if (other.elements != null)
				return false;
		} else if (!elements.equals(other.elements))
			return false;
		return true;
	}

}
