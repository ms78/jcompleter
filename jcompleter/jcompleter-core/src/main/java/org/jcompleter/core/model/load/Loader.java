package org.jcompleter.core.model.load;

import java.util.Collection;

import org.jcompleter.core.model.Completable;

/**
 * This interface should be implemented by all data loaders.
 * 
 * @author Muthanna Alshoufi
 *
 */
public interface Loader {

	/**
	 * Provides a collection of elements to build the completer with.
	 * 
	 * @return a collection of elements
	 */
	Collection<? extends Completable> getElements();
}