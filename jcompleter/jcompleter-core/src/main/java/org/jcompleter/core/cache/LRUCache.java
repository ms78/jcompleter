package org.jcompleter.core.cache;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A simple cache implementation based on {@link LinkedHashMap}. This
 * implementation adopts a Least-Recently-Used evacuation policy and uses the
 * underlying {@link LinkedHashMap#removeEldestEntry} to evict undesired
 * entries.
 *
 * @param <K>
 *            type of the cache keys
 * @param <V>
 *            type of the cache values
 * 
 * @author Muthanna Alshoufi
 */
class LRUCache<K, V> implements Cache<K, V> {

	static final float DEFAULT_LOAD_FACTOR = 0.70f;

	private final Map<K, V> cacheMap;

	LRUCache(final int cacheSize) {
		int linkedHashMapCapacity = (int) Math.ceil(cacheSize / DEFAULT_LOAD_FACTOR) + 1;

		LinkedHashMap<K, V> linkedMap = new LinkedHashMap<K, V>(linkedHashMapCapacity, DEFAULT_LOAD_FACTOR, true) {

			private static final long serialVersionUID = 1L;

			@Override
			protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
				return size() > cacheSize;
			}
		};

		cacheMap = Collections.synchronizedMap(linkedMap);
	}

	@Override
	public V get(K k) {
		return cacheMap.get(k);
	}

	@Override
	public void put(K k, V v) {
		cacheMap.put(k, v);
	}

	@Override
	public V remove(K k) {
		return cacheMap.remove(k);
	}

	@Override
	public boolean containsKey(K k) {
		return cacheMap.containsKey(k);
	}

	@Override
	public boolean isEmpty() {
		return cacheMap.isEmpty();
	}

	@Override
	public void flush() {
		cacheMap.clear();
	}

	@Override
	public int getSize() {
		return cacheMap.size();
	}

	@Override
	public Map<K, V> getAll() {
		return Collections.unmodifiableMap(cacheMap);
	}

}
