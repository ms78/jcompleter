package org.jcompleter.core.exception;

/**
 * Initialization Exception thrown by the JCompleter components when it fails
 * during an initialization operation.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterInitializationException extends CompleterException {

	private static final long serialVersionUID = 1L;

	public CompleterInitializationException(String s) {
		super(s);
	}

	public CompleterInitializationException(Throwable e) {
		super(e);
	}

	public CompleterInitializationException(String s, Throwable e) {
		super(s, e);
	}
}
