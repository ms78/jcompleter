package org.jcompleter.core.model.load;

/**
 * A loader factory to allow creating {@link Loader} implementations.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface LoaderFactory {

	/**
	 * Creates instances of {@link Loader} implementations.
	 * 
	 * @return A {@link Loader} implementation.
	 */
	Loader create();

}
