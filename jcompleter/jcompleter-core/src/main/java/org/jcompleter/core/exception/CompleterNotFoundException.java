package org.jcompleter.core.exception;

/**
 * Exception thrown by JCompleter when the caller tries to use an undefined
 * completer.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterNotFoundException extends CompleterException {

	private static final long serialVersionUID = 1L;

	public CompleterNotFoundException(String s) {
		super(s);
	}

	public CompleterNotFoundException(Throwable e) {
		super(e);
	}

	public CompleterNotFoundException(String s, Throwable e) {
		super(s, e);
	}
}
