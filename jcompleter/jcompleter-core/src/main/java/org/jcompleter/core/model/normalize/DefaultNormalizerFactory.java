package org.jcompleter.core.model.normalize;

import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

/**
 * Creates {@link CompositeNormalizer} instances.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class DefaultNormalizerFactory implements NormalizerFactory {

	/**
	 * Creates a {@link CompositeNormalizer} implementation instance composed
	 * of:
	 * <ul>
	 * <li>{@link WhitespaceNormalizer}
	 * <li>{@link DiacriticsNormalizer}
	 * <li>{@link CaseNormalizer}
	 * </ul>
	 */
	@Override
	public Normalizer create(Locale locale) {
		Set<Normalizer> normalizers = new LinkedHashSet<Normalizer>();

		// First eliminate whitespace chars
		normalizers.add(new WhitespaceNormalizer());

		// Harmonize accented chars
		normalizers.add(new DiacriticsNormalizer());

		// Harmonize the case
		normalizers.add(new CaseNormalizer(locale));

		return new CompositeNormalizer(normalizers);
	}

}
