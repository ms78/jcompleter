package org.jcompleter.core.model.tokenize;

import java.util.Locale;

/**
 * Creates {@link Tokenizer} instances.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface TokenizerFactory {

	/**
	 * Create {@link Tokenizer} implementations.
	 * 
	 * @param locale
	 *            the locale to use
	 * @return {@link Tokenizer} instance
	 */
	Tokenizer create(Locale locale);

}
