package org.jcompleter.core.cache;

/**
 * A factory to create {@link Cache} objects.
 * 
 * @author Muthanna Alshoufi
 *
 */
public interface CacheFactory<K, V> {

	/**
	 * Creates default {@link Cache} implementation with a maximum number of
	 * elements determined by the cache size parameter. the default cache
	 * created from this method is {@link LRUCache}. If you want another
	 * implementation, you will have to override this method in your sub-class.
	 * 
	 * @param cacheSize
	 *            the max number of elements allowed in the cache
	 * @return a {@link Cache} instance
	 */
	Cache<K, V> create(int cacheSize);

}
