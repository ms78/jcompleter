package org.jcompleter.core.model.sort;

import java.util.Locale;

import org.jcompleter.core.model.Element;

/**
 * Default factory used to provide sorters.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class DefaultSorterFactory<E extends Element> implements SorterFactory<E> {

	@Override
	public Sorter<E> create(Locale locale, SorterType sorterType) {
		Sorter<E> sorter = null;
		switch (sorterType) {
		case LEXICOGRAPHIC:
			sorter = new LexicographicSorter<E>(locale);
			break;
		case SCORE:
			sorter = new ScoreSorter<E>();
			break;
		default:
			sorter = new LexicographicSorter<E>(locale);
			break;
		}
		
		return sorter;
	}

}
