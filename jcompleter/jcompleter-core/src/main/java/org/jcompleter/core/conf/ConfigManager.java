package org.jcompleter.core.conf;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jcompleter.core.exception.CompleterConfigurationException;
import org.jcompleter.core.util.CommonUtil;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Initializes all completers configured in JCompleter. The configuration is
 * loaded from an XML file "jcompleter.xml" by default. At startup, JCompleter
 * searches the class path and tries to parse the configuration XML file. You
 * can also pass your own XML config file if you don't want to use the default
 * one.
 * 
 * @author Muthanna Alshoufi
 */
public class ConfigManager {

	private static final Logger logger = Logger.getLogger(ConfigManager.class.getName());

	// Default configuration file
	private static final String DEFAULT_CONFIG_FILE = "jcompleter.xml";
	private final static String XML_COMPLETER_PARENT_NODE = "completer";
	private final static String XML_COMPLETER_NAME = "name";
	private final static String XML_COMPLETER_NUMBER_OF_QUERIES_TO_CACHE = "numberOfQueriesToCache";
	private final static String XML_COMPLETER_MINIMUM_QUERY_LENGTH_TO_CACHE = "minimumQueryLengthToCache";
	private final static String XML_COMPLETER_MAXIMUM_QUERY_LENGTH_TO_CACHE = "maximumQueryLengthToCache";

	private final static String XML_COMPLETER_LOADER_FACTORY = "loaderFactory";
	private final static String XML_COMPLETER_TOKENIZER_FACTORY = "tokenizerFactory";
	private final static String XML_COMPLETER_INDEXER_FACTORY = "indexerFactory";
	private final static String XML_COMPLETER_NORMALIZER_FACTORY = "normalizerFactory";
	private final static String XML_COMPLETER_SORTER_FACTORY = "sorterFactory";

	private Config config;

	public ConfigManager() throws CompleterConfigurationException {
		this(DEFAULT_CONFIG_FILE);
	}

	public ConfigManager(final String configFile) throws CompleterConfigurationException {
		if (CommonUtil.isBlank(configFile)) {
			throw new CompleterConfigurationException(new IllegalArgumentException(
					"No configuration file in the class path. Can't initialize JCompleter."));
		}

		InputStream is = null;
		try {
			is = this.getClass().getClassLoader().getResourceAsStream(configFile);
			initialize(is);
		} catch (Exception e) {
			throw new CompleterConfigurationException("Couldn't initialize JCompleter from file:" + configFile, e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException ioe) {
					logger.log(Level.WARNING,
							"Failed to close the input stream while initializing JCompleter from config file.", ioe);
				}
			}
		}
	}

	public Config getConfig() {
		return config;
	}

	private void initialize(final InputStream is) throws CompleterConfigurationException {
		try {
			DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document doc = builder.parse(is);
			this.config = Config.valueOf(parseConfigEntries(doc));
		} catch (Exception e) {
			throw new CompleterConfigurationException("Something went wrong while trying to initialize Config Manager.",
					e);
		}
	}

	private static Set<ConfigEntry> parseConfigEntries(Document doc) {
		Set<ConfigEntry> configEntries = new HashSet<ConfigEntry>();

		NodeList nodes = doc.getElementsByTagName(XML_COMPLETER_PARENT_NODE);
		for (int i = 0; i < nodes.getLength(); i++) {
			NamedNodeMap attrs = nodes.item(i).getAttributes();

			// Completer name
			String name = getAsString(attrs, XML_COMPLETER_NAME);
			if (CommonUtil.isBlank(name) || isNameAlreadyUsed(name, configEntries)) {
				logger.severe("While parsing JCompleter XML config, it found an invalid mandatory attribute: "
						+ XML_COMPLETER_NAME + " with value: " + name);
				continue;
			}

			// Factories
			String loaderFactory = getAsString(attrs, XML_COMPLETER_LOADER_FACTORY);
			if (CommonUtil.isBlank(loaderFactory)) {
				logger.severe("While parsing JCompleter XML config, we found an invalid mandatory attribute: "
						+ XML_COMPLETER_LOADER_FACTORY);
				continue;
			}

			String tokenizerFactory = getAsString(attrs, XML_COMPLETER_TOKENIZER_FACTORY);
			String indexerFactory = getAsString(attrs, XML_COMPLETER_INDEXER_FACTORY);
			String sorterFactory = getAsString(attrs, XML_COMPLETER_SORTER_FACTORY);
			String normalizerFactory = getAsString(attrs, XML_COMPLETER_NORMALIZER_FACTORY);

			// Cache
			// numberOfQueriesToCache
			Integer numberOfQueriesToCache = getAsInt(attrs, XML_COMPLETER_NUMBER_OF_QUERIES_TO_CACHE);

			// minimumQueryLengthToCache
			Integer minimumQueryLengthToCache = getAsInt(attrs, XML_COMPLETER_MINIMUM_QUERY_LENGTH_TO_CACHE);

			// minimumQueryLengthToCache
			Integer maximumQueryLengthToCache = getAsInt(attrs, XML_COMPLETER_MAXIMUM_QUERY_LENGTH_TO_CACHE);

			ConfigEntry configEntry = new ConfigEntry().setName(name).setNumberOfQueriesToCache(numberOfQueriesToCache)
					.setMinimumQueryLengthToCache(minimumQueryLengthToCache)
					.setMaximumQueryLengthToCache(maximumQueryLengthToCache).setLoaderFactory(loaderFactory)
					.setTokenizerFactory(tokenizerFactory).setIndexerFactory(indexerFactory)
					.setSorterFactory(sorterFactory).setNormalizerFactory(normalizerFactory);

			configEntries.add(configEntry);
		}

		return configEntries;
	}

	private static boolean validate(Node node) {
		return (node != null && node.getNodeValue() != null);
	}

	private static boolean isNameAlreadyUsed(String name, Set<ConfigEntry> configEntries) {
		for (ConfigEntry entry : configEntries) {
			if (name.equalsIgnoreCase(entry.getName()))
				return true;
		}

		return false;
	}

	private static String getAsString(NamedNodeMap attrs, String attrName) {
		String value = null;
		if (validate(attrs.getNamedItem(attrName))) {
			value = attrs.getNamedItem(attrName).getNodeValue();
		}

		return value;
	}

	private static Integer getAsInt(NamedNodeMap attrs, String attrName) {
		Integer value = null;
		if (validate(attrs.getNamedItem(attrName))) {
			try {
				value = Integer.parseInt(attrs.getNamedItem(attrName).getNodeValue());
			} catch (NumberFormatException nfe) {
				logger.log(Level.SEVERE, attrName + " is not a number.", nfe);
			}
		}

		return value;
	}

}
