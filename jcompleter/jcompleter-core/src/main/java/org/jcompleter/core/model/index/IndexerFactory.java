package org.jcompleter.core.model.index;

/**
 * Creates {@link Indexer} instances.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface IndexerFactory<E> {

	Indexer<E> create();

}
