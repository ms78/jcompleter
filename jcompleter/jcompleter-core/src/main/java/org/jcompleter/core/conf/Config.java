package org.jcompleter.core.conf;

import java.util.Set;

/**
 * A representation of the configuration used by JCompleter; mainly an image of
 * the XML configuration file "jcompleter.xml" by default.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class Config {

	public static Config valueOf(Set<ConfigEntry> configEntries) {
		return new Config(configEntries);
	}

	private final Set<ConfigEntry> configEntries;

	private Config(Set<ConfigEntry> configEntries) {
		this.configEntries = configEntries;
	}

	public Set<ConfigEntry> getEntries() {
		return configEntries;
	}

}
