package org.jcompleter.core.model.api.request;

import org.jcompleter.core.model.api.ApiMessage;

/**
 * Super-class of all requests.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public abstract class Request extends ApiMessage {

	private static final long serialVersionUID = 1L;

	/**
	 * Checks whether the request is valid or no.
	 * 
	 * @return true if valid, false otherwise
	 */
	public abstract boolean isValid();

}
