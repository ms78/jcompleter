package org.jcompleter.core.model.normalize;

import java.util.Locale;

/**
 * Normalizes the String case. It is {@link Locale} aware and thus the target
 * Locale has to be passed in argument to get the expected result.
 * 
 * @author Muthanna Alshoufi
 *
 */
class CaseNormalizer implements Normalizer {

	private final Locale locale;

	CaseNormalizer(Locale locale) {
		this.locale = locale;
	}

	@Override
	public String normalize(String input) {
		if (input != null)
			return input.toLowerCase(locale);

		return null;
	}

}
