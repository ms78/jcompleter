package org.jcompleter.core.exception;

/**
 * Parent exception of all exceptions thrown by JCompleter.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterException extends Exception {

	private static final long serialVersionUID = 1L;

	public CompleterException(String s) {
		super(s);
	}

	public CompleterException(Throwable e) {
		super(e);
	}

	public CompleterException(String s, Throwable e) {
		super(s, e);
	}
}
