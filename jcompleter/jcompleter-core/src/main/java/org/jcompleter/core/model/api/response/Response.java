package org.jcompleter.core.model.api.response;

import org.jcompleter.core.model.api.ApiMessage;
import org.jcompleter.core.model.api.request.Request;

/**
 * Super-class of all responses.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public abstract class Response extends ApiMessage {

	private static final long serialVersionUID = 1L;

	private final Request request;

	public Response(Request request) {
		this.request = request;
	}

	/**
	 * Gets the original request that generated this response.
	 * 
	 * @return the original request
	 */
	public Request getRequest() {
		return request;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Response other = (Response) obj;
		if (request == null) {
			if (other.request != null)
				return false;
		} else if (!request.equals(other.request))
			return false;
		return true;
	}

}
