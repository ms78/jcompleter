package org.jcompleter.core.cache;

import java.util.Map;

/**
 * A generic cache interface to be used by JCompleter to cache search results.
 * It is designed to be typed by a key of type K and a value of type V and thus
 * gives flexibility to define multiple cache instances with different key/value
 * type combination.
 *
 * @param <K>
 *            The generic key type
 * @param <V>
 *            The generic value type
 * 
 * @author Muthanna Alshoufi
 */
public interface Cache<K, V> {

	/**
	 * Puts an element in the cache using its unique key.
	 * 
	 * @param k
	 *            the unique key identifying the element
	 * @param v
	 *            the element to be cached
	 */
	void put(K k, V v);

	/**
	 * Fetches an element from the cache using its unique key.
	 * 
	 * @param k
	 *            the unique key identifying the element
	 * @return an element
	 */
	V get(K k);

	/**
	 * Removes an element from the cache using its unique key.
	 * 
	 * @param k
	 *            the unique key identifying the element
	 * @return the removed value
	 */
	V remove(K k);

	/**
	 * Checks whether the cache has an element for the passed key.
	 * 
	 * @param k
	 *            the unique key identifying the element
	 * @return true if the key is present
	 */
	boolean containsKey(K k);

	/**
	 * Checks whether the cache is empty.
	 * 
	 * @return true if empty
	 */
	boolean isEmpty();

	/**
	 * Returns the total number of elements in this cache.
	 * 
	 * @return an integer representing the number of elements
	 */
	int getSize();

	/**
	 * Gets a view map of all the cached entries.
	 * 
	 * @return a view map representing all the entries
	 */
	Map<K, V> getAll();

	/**
	 * Clears all cache elements.
	 */
	void flush();

}
