package org.jcompleter.core.model.sort;

/**
 * Enumerates the sorter types supported by JCompleter.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public enum SorterType {

	LEXICOGRAPHIC, SCORE;

	public static SorterType fromValue(String value) {
		for (SorterType type : values()) {
			if (type.name().equalsIgnoreCase(value))
				return type;
		}

		return LEXICOGRAPHIC;
	}

}
