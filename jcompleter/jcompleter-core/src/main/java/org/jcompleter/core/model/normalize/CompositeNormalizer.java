package org.jcompleter.core.model.normalize;

import java.util.Set;

/**
 * A composite implementation of {@link Normalizer} to allow applying a list of
 * normalizers on the target input String.
 * 
 * @author Muthanna Alshoufi
 */
class CompositeNormalizer implements Normalizer {

	private Set<Normalizer> normalizers;

	CompositeNormalizer(Set<Normalizer> normalizers) {
		this.normalizers = normalizers;
	}

	@Override
	public String normalize(String input) {
		if (input == null)
			return null;

		String output = input;
		for (Normalizer n : normalizers)
			output = n.normalize(output);

		return output;
	}

}
