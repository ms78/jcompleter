package org.jcompleter.core.external;

/**
 * Helps bridge to external libraries such as Spring. Implementations must
 * provide a way to load the beans defined in their container.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface JCompleterContextBridge {

	<T> T getBean(Class<T> beanClass);
}