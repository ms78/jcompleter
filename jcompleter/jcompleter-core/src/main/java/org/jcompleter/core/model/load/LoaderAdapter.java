package org.jcompleter.core.model.load;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.Completer;
import org.jcompleter.core.model.Element;
import org.jcompleter.core.util.CommonUtil;

/**
 * Wraps {@link Completable} isntances provided by the caller into
 * {@link Element} instances to be fed to {@link Completer}.
 *
 * @param <E>
 *            the type of elements
 * 
 * @author Muthanna Alshoufi
 */
public class LoaderAdapter<E> {

	private static final Logger logger = Logger.getLogger(LoaderAdapter.class.toString());

	private final Loader loader;

	public LoaderAdapter(Loader loader) {
		this.loader = loader;
	}

	@SuppressWarnings("unchecked")
	public Map<Locale, Collection<E>> getElements() {

		Map<Locale, Collection<E>> elementsMap = new HashMap<Locale, Collection<E>>();

		Collection<? extends Completable> userData = loader.getElements();

		if(userData != null && !userData.isEmpty()) {
			for (Completable c : userData) {
				if (!validate(c))
					continue;

				Locale locale = c.getCompletableLocale() == null ? Locale.ENGLISH : c.getCompletableLocale();

				if (elementsMap.get(locale) == null)
					elementsMap.put(locale, new ArrayList<E>());

				E element = (E) new Element(c);
				elementsMap.get(locale).add(element);
			}
		}
		
		return elementsMap;
	}

	private boolean validate(Completable completable) {
		boolean valid = true;
		if (CommonUtil.isBlank(completable.getCompletableText())) {
			valid = false;
			logger.warning(
					"You passed a completable with a blank text. This element will be thus ignored.\n" + completable);
		} else if (CommonUtil.isBlank(completable.getCompletableId())) {
			valid = false;
			logger.warning(
					"You passed a completable with a blank ID. This element will be thus ignored.\n" + completable);
		}

		return valid;
	}

}
