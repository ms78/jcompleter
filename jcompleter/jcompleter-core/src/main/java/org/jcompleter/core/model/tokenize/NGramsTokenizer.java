package org.jcompleter.core.model.tokenize;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jcompleter.core.util.CommonUtil;

/**
 * Tokenizes Strings into NGrams.
 * 
 * @author Muthanna Alshoufi
 * 
 */
class NGramsTokenizer extends WordTokenizer implements Tokenizer {

	private final static int DEFAULT_SIZE = 3;
	private final int size;

	NGramsTokenizer(Locale locale) {
		this(DEFAULT_SIZE, locale);
	}
	
	NGramsTokenizer(int size, Locale locale) {
		super(locale);
		this.size = size;
	}

	@Override
	public Map<String, List<String>> tokenize(String input) {
		Map<String, List<String>> result = new LinkedHashMap<String, List<String>>();
		if (!CommonUtil.isBlank(input)) {
			for (Map.Entry<String, List<String>> entry : super.tokenize(input).entrySet()) {
				for (String word : entry.getValue()) {
					List<String> parts = new LinkedList<String>();
					int finalSize = word.length() < size ? word.length() : size;
					for (int i = 0; i + finalSize <= word.length(); i++) {
						parts.add(word.substring(i, i + finalSize));
					}
					result.put(word, parts);
				}
			}
		}

		return result;
	}

}
