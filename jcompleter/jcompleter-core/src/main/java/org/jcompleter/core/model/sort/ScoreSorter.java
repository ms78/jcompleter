package org.jcompleter.core.model.sort;

import org.jcompleter.core.model.Element;

/**
 * A score descendant based sorter.
 * 
 * @author Muthanna Alshoufi
 *
 * @param <E>
 *            the type of elements
 */
class ScoreSorter<E extends Element> implements Sorter<E> {

	ScoreSorter() {
	}

	@Override
	public int compare(E e1, E e2) {
		return e2.getCompletableScore().compareTo(e1.getCompletableScore());
	}

}
