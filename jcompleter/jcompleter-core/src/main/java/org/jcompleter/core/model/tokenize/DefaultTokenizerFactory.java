package org.jcompleter.core.model.tokenize;

import java.util.Locale;

/**
 * Creates {@link WordTokenizer} implementations of {@link Tokenizer} interface.
 * 
 * @author Muthanna Alshoufi
 */
public class DefaultTokenizerFactory implements TokenizerFactory {

	/**
	 * Creates a {@link WordTokenizer} implementation of {@link Tokenizer}.
	 * 
	 * @param locale
	 *            the locale to use
	 * @return a {@link WordTokenizer} instance
	 */
	@Override
	public Tokenizer create(Locale locale) {
		return new WordTokenizer(locale);
	}

}
