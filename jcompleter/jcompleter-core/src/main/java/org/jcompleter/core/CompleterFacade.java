package org.jcompleter.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jcompleter.core.conf.Config;
import org.jcompleter.core.conf.ConfigEntry;
import org.jcompleter.core.conf.ConfigManager;
import org.jcompleter.core.conf.GenericFactory;
import org.jcompleter.core.exception.CompleterConfigurationException;
import org.jcompleter.core.exception.CompleterException;
import org.jcompleter.core.exception.CompleterInitializationException;
import org.jcompleter.core.exception.CompleterNotFoundException;
import org.jcompleter.core.model.Completer;
import org.jcompleter.core.model.Element;
import org.jcompleter.core.model.api.request.AutocompleteRequest;
import org.jcompleter.core.model.api.request.Request;
import org.jcompleter.core.model.api.response.AutocompleteResponse;
import org.jcompleter.core.model.api.response.Response;
import org.jcompleter.core.model.index.IndexerFactory;
import org.jcompleter.core.model.load.LoaderFactory;
import org.jcompleter.core.model.normalize.NormalizerFactory;
import org.jcompleter.core.model.sort.SorterFactory;
import org.jcompleter.core.model.sort.SorterType;
import org.jcompleter.core.model.tokenize.TokenizerFactory;
import org.jcompleter.core.util.CommonUtil;

/**
 * The main facade to interface with the autocompleters in JCompleter.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterFacade {

	private static final Logger logger = Logger.getLogger(CompleterFacade.class.toString());

	protected final Map<String, Completer<Element>> completers;

	/**
	 * Constructs a new completer facade. It will try to initialize the facade from
	 * the XML config file if configured. Otherwise, it will initialize the facade
	 * with no completers. You are expected to add new completers programmatically
	 * use the following method {@link CompleterFacade#setCompleters(List)}.
	 * 
	 * @throws CompleterInitializationException if initializing JCompleter has failed
	 */
	public CompleterFacade() throws CompleterInitializationException {
		completers = new HashMap<String, Completer<Element>>();
		logger.info("Initializing JCompleter Facade ...");

		Config config = null;
		try {
			ConfigManager configManager = new ConfigManager();
			config = configManager.getConfig();
		} catch (CompleterConfigurationException e) {
			logger.log(Level.WARNING, "Could not initialize JCompleter from XML config file", e);
		}
		
		if (config != null && config.getEntries() != null) {

			// Build all defined completers
			for (ConfigEntry configEntry : config.getEntries()) {
				LoaderFactory loaderFactory = null;
				if (!CommonUtil.isBlank(configEntry.getLoaderFactory())) {
					GenericFactory<LoaderFactory> loaderFactoryGeneric = new GenericFactory<LoaderFactory>();
					loaderFactory = loaderFactoryGeneric.create(configEntry.getLoaderFactory(), LoaderFactory.class);
				}

				TokenizerFactory tokenizerFactory = null;
				if (!CommonUtil.isBlank(configEntry.getTokenizerFactory())) {
					GenericFactory<TokenizerFactory> tokenizerFactoryGeneric = new GenericFactory<TokenizerFactory>();
					tokenizerFactory = tokenizerFactoryGeneric.create(configEntry.getTokenizerFactory(),
							TokenizerFactory.class);
				}

				IndexerFactory<Element> indexerFactory = null;
				if (!CommonUtil.isBlank(configEntry.getIndexerFactory())) {
					GenericFactory<IndexerFactory<Element>> indexerFactoryGeneric = new GenericFactory<IndexerFactory<Element>>();
					indexerFactory = indexerFactoryGeneric.create(configEntry.getIndexerFactory(),
							IndexerFactory.class);
				}

				NormalizerFactory normalizerFactory = null;
				if (!CommonUtil.isBlank(configEntry.getNormalizerFactory())) {
					GenericFactory<NormalizerFactory> normalizerFactoryGeneric = new GenericFactory<NormalizerFactory>();
					normalizerFactory = normalizerFactoryGeneric.create(configEntry.getNormalizerFactory(),
							NormalizerFactory.class);
				}

				SorterFactory<Element> sorterFactory = null;
				if (!CommonUtil.isBlank(configEntry.getSorterFactory())) {
					GenericFactory<SorterFactory<Element>> sorterFactoryGeneric = new GenericFactory<SorterFactory<Element>>();
					sorterFactory = sorterFactoryGeneric.create(configEntry.getSorterFactory(), SorterFactory.class);
				}

				Completer<Element> completer = new Completer<Element>().setName(configEntry.getName())
						.setNumberOfQueriesToCache(configEntry.getNumberOfQueriesToCache())
						.setMinimumQueryLengthToCache(configEntry.getMinimumQueryLengthToCache())
						.setMaximumQueryLengthToCache(configEntry.getMaximumQueryLengthToCache())
						.setLoaderFactory(loaderFactory).setTokenizerFactory(tokenizerFactory)
						.setIndexerFactory(indexerFactory).setNormalizerFactory(normalizerFactory)
						.setSorterFactory(sorterFactory).build();

				register(completer);
			}
			logger.info("JCompleter Facade has been successfully initialized from config file.");
		} else {
			logger.info("JCompleter Facade has been successfully initialized for programmatical usage.");
		}

	}

	public Collection<Completer<Element>> getCompleters() {
		return completers.values();
	}

	public void setCompleters(List<Completer<Element>> completers) throws CompleterInitializationException {
		for (Completer<Element> completer : completers)
			register(completer);
	}

	protected void register(Completer<Element> completer) throws CompleterInitializationException {
		if (completer == null)
			throw new CompleterInitializationException("You are trying to register an invlid completer.");

		String name = completer.getName();
		if (completers.containsKey(name)) {
			String msg = name + " is already used. Use another name.";
			logger.severe(msg);
			throw new CompleterInitializationException(msg);
		}

		completers.put(name, completer);
	}

	public AutocompleteResponse autocomplete(String completerName, String query, Locale locale, Integer numberOfResultsLimit,
			SorterType sorterType) throws CompleterException {
		AutocompleteRequest request = new AutocompleteRequest(query, locale, numberOfResultsLimit, sorterType);
		return (AutocompleteResponse) send(completerName, request);
	}
	
	public Response send(String completerName, Request request) throws CompleterException {
		return lookup(completerName).process(request);
	}
	
	private Completer<Element> lookup(String completerName) throws CompleterException {
		Completer<Element> completer = completers.get(completerName);
		if (completer == null) {
			throw new CompleterNotFoundException(
					completerName + " does not belong to the list of completers you defined.");
		}
		
		return completer;
	}

	public void close() {
		for (Completer<Element> completer : completers.values()) {
			completer.destroy();
		}
	}

}
