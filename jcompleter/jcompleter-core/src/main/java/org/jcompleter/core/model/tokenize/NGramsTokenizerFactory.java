package org.jcompleter.core.model.tokenize;

import java.util.Locale;

/**
 * Creates {@link NGramsTokenizer} instances;
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class NGramsTokenizerFactory implements TokenizerFactory {

	/**
	 * Created a {@link NGramsTokenizer} implementation of {@link Tokenizer}.
	 */
	@Override
	public Tokenizer create(Locale locale) {
		return new NGramsTokenizer(locale);
	}

}
