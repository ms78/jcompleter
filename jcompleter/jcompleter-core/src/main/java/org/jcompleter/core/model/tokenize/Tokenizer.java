package org.jcompleter.core.model.tokenize;

import java.util.List;
import java.util.Map;

/**
 * Tokenizes Strings into smaller parts. Different implementations will
 * determine the tokenization strategy.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface Tokenizer {

	/**
	 * Tokenizes the input String into smaller parts. The tokenization mechanism
	 * is determined by the concrete implementations of this interface.
	 * 
	 * @param input
	 *            the String to be tokenized
	 * @return a map holding tokenized parts
	 */
	Map<String, List<String>> tokenize(String input);

}
