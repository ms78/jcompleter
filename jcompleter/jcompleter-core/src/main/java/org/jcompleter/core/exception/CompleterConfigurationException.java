package org.jcompleter.core.exception;

/**
 * Configuration Exception thrown by the JCompleter components when it fails
 * during configuration.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterConfigurationException extends CompleterException {

	private static final long serialVersionUID = 1L;

	public CompleterConfigurationException(String s) {
		super(s);
	}

	public CompleterConfigurationException(Throwable e) {
		super(e);
	}

	public CompleterConfigurationException(String s, Throwable e) {
		super(s, e);
	}
}
