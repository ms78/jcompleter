package org.jcompleter.core.model;

import java.util.Locale;

/**
 * JCompleter core element to wrap completable user's entities and provide a
 * null-safe wrapper.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class Element implements Completable, Comparable<Element> {

	private static final long serialVersionUID = 1L;

	private static final Double ZERO = 0.0d;

	private final String uniqueId;
	private final Completable value;

	/**
	 * The number of times this element has been included in the auto-completion
	 * result
	 */
	private volatile long numberOfHits;

	public Element(final Completable value) {
		this.value = value;
		this.uniqueId = makeUniqueId();
	}

	public Completable getValue() {
		return value;
	}

	public Double getCompletableScore() {
		return value.getCompletableScore() == null ? ZERO : value.getCompletableScore();
	}

	@Override
	public String getCompletableId() {
		return value.getCompletableId();
	}

	public String getUniqueId() {
		return uniqueId;
	}

	@Override
	public String getCompletableText() {
		return value.getCompletableText();
	}

	public long getNumberOfHits() {
		return numberOfHits;
	}

	public void setNumberOfHits(long numberOfHits) {
		this.numberOfHits = numberOfHits;
	}

	public Locale getCompletableLocale() {
		return value.getCompletableLocale() == null ? Locale.ENGLISH : value.getCompletableLocale();
	}

	protected String makeUniqueId() {
		return (value != null) ? value.getClass().getName() + "_" + getCompletableLocale().getLanguage() + "_"
				+ value.getCompletableId() : "";
	}

	@Override
	public int compareTo(Element o) {
		return uniqueId.compareTo(o.uniqueId);
	}

}
