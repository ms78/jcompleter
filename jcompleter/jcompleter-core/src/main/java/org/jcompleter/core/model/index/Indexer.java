package org.jcompleter.core.model.index;

import java.util.Map;
import java.util.Set;

/**
 * The main interface for a data index in JCompleter.
 * 
 * @author Muthanna Alshoufi
 *
 * @param <E>
 *            the type of elements
 */
public interface Indexer<E> {

	/**
	 * Adds a new element in the index.
	 * 
	 * @param term
	 *            the term to attach the element to
	 * @param element
	 *            the element to be added
	 */
	void add(String term, E element);

	/**
	 * Fetches all terms
	 * 
	 * @return a set of all terms in index
	 */
	Set<String> getAllTerms();

	/**
	 * Fetches all the elements hosted in the index
	 * 
	 * @return a set of all values in index
	 */
	Set<E> getAllElements();

	/**
	 * Gets a view of all the terms and elements.
	 * 
	 * @return a view map whose keys are terms and values are elements
	 */
	Map<String, Set<E>> getAllTermsAndElements();

	/**
	 * Gets result for the query and locale
	 * 
	 * @param term
	 *            the term to lookup
	 * @return a set of elements matching the query
	 */
	Set<E> lookup(String term);

	/**
	 * Removes the term from the index.
	 * 
	 * @param term
	 *            the term to be removed
	 * @return the set of elements that were attached to the term
	 */
	Set<E> remove(String term);

	/**
	 * Removes all the elements indicated in the argument.
	 * 
	 * @param elements
	 *            the elements to remove
	 * @return true if
	 */
	boolean removeAll(Set<E> elements);

	/**
	 * Clears elements from this index.
	 */
	void clear();

}
