package org.jcompleter.core.model.sort;

import java.util.Locale;

/**
 * Creates {@link Sorter} instances.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface SorterFactory<E> {

	/**
	 * Creates the default sorter.
	 * 
	 * @param locale
	 *            the locale to consider in the sorter
	 * @param sorterType
	 *            the type of sorter to create
	 * @return a {@link Sorter} instance
	 */
	Sorter<E> create(Locale locale, SorterType sorterType);

}
