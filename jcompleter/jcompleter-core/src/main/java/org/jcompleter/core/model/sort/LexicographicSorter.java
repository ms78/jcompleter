package org.jcompleter.core.model.sort;

import java.text.Collator;
import java.util.Locale;

import org.jcompleter.core.model.Element;

/**
 * A lexicographical sorter.
 * 
 * @author Muthanna Alshoufi
 *
 * @param <E>
 *            the type of elements
 */
class LexicographicSorter<E extends Element> implements Sorter<E> {

	private final Locale locale;

	LexicographicSorter(Locale locale) {
		this.locale = locale;
	}

	@Override
	public int compare(E e1, E e2) {
		return Collator.getInstance(locale).compare(e1.getCompletableText(), e2.getCompletableText());
	}

}
