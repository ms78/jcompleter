package org.jcompleter.core.model.api.request;

import java.util.Locale;

import org.jcompleter.core.model.sort.SorterType;
import org.jcompleter.core.util.CommonUtil;

/**
 * The main autocompleter implementation of {@link Request}.
 * 
 * @author Muthanna Alshoufi
 *
 */
public class AutocompleteRequest extends Request {

	private static final long serialVersionUID = 1L;

	private final String query;
	private final Locale locale;
	private final Integer numberOfResults;
	private final SorterType sorterType;

	public AutocompleteRequest(String query, Locale locale) {
		this(query, locale, null);
	}

	public AutocompleteRequest(String query, Locale locale, Integer numberOfResultsLimit) {
		this(query, locale, numberOfResultsLimit, SorterType.LEXICOGRAPHIC);
	}

	public AutocompleteRequest(String query, Locale locale, Integer numberOfResults, SorterType sorterType) {
		this.query = query;
		this.locale = locale == null ? Locale.ENGLISH : locale;
		this.numberOfResults = numberOfResults;
		this.sorterType = sorterType;
	}

	/**
	 * Gets the original query sent by the client.
	 * 
	 * @return a String representing the original query
	 */
	public String getQuery() {
		return query;
	}

	/**
	 * Gets the locale of this request.
	 * 
	 * @return a locale representing the language, country, and region
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * Gets the max number of results to return to the caller.
	 * 
	 * @return an integer representing the number of results
	 */
	public Integer getNumberOfResults() {
		return numberOfResults;
	}

	/**
	 * Gets the {@link SorterType} for this request.
	 * 
	 * @return an enumerated value of type {@link SorterType}
	 */
	public SorterType getSorterType() {
		return sorterType;
	}

	@Override
	public boolean isValid() {
		return !CommonUtil.isBlank(query);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((locale == null) ? 0 : locale.hashCode());
		result = prime * result + ((numberOfResults == null) ? 0 : numberOfResults.hashCode());
		result = prime * result + ((query == null) ? 0 : query.hashCode());
		result = prime * result + ((sorterType == null) ? 0 : sorterType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutocompleteRequest other = (AutocompleteRequest) obj;
		if (locale == null) {
			if (other.locale != null)
				return false;
		} else if (!locale.equals(other.locale))
			return false;
		if (numberOfResults == null) {
			if (other.numberOfResults != null)
				return false;
		} else if (!numberOfResults.equals(other.numberOfResults))
			return false;
		if (query == null) {
			if (other.query != null)
				return false;
		} else if (!query.equals(other.query))
			return false;
		if (sorterType != other.sorterType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AutocompleteRequest [query=" + query + ", locale=" + locale + ", numberOfResults=" + numberOfResults
				+ ", sorterType=" + sorterType + ", date=" + date + "]";
	}

}
