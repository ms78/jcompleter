package org.jcompleter.core.util;

import java.util.Locale;
import java.util.MissingResourceException;

/**
 * A set of common utilities.
 * 
 * @author Muthanna Alshoufi
 *
 */
public final class CommonUtil {

	private CommonUtil() {
	}

	/**
	 * Tells whether a String is blank.
	 * 
	 * @param s
	 *            the input String
	 * @return true if blank, false if not
	 */
	public static boolean isBlank(String s) {
		return s == null || s.trim().isEmpty();
	}

	public static Locale parseLocale(String locale) {
		Locale finalLocale = null;
		if (!isBlank(locale)) {
			String[] parts = locale.split("_");
			switch (parts.length) {
			case 1:
				finalLocale = new Locale(parts[0]);
				break;
			case 2:
				finalLocale = new Locale(parts[0], parts[1]);
				break;
			case 3:
				finalLocale = new Locale(parts[0], parts[1], parts[2]);
				break;
			default:
				finalLocale = null;
				break;
			}
		}

		return finalLocale;
	}

	public static boolean isValid(Locale locale) {
		boolean valid = false;
		if (locale != null) {
			try {
				valid = locale.getISO3Language() != null;
				valid |= locale.getISO3Country() != null;
			} catch (MissingResourceException e) {
			}
		}

		return valid;
	}

}
