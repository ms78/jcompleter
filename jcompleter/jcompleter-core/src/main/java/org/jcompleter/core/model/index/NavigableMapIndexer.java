package org.jcompleter.core.model.index;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * An indexer implementation based on {@link ConcurrentSkipListMap}.
 * 
 * @author Muthanna Alshoufi
 *
 */
class NavigableMapIndexer<E> implements Indexer<E>, Serializable {

	private static final long serialVersionUID = 1L;

	protected ConcurrentNavigableMap<String, Set<E>> tree;

	NavigableMapIndexer() {
		this(null);
	}

	NavigableMapIndexer(Comparator<String> comparator) {
		this.tree = comparator != null ? new ConcurrentSkipListMap<String, Set<E>>(comparator)
				: new ConcurrentSkipListMap<String, Set<E>>();
	}

	@Override
	public void add(String term, E e) {
		if (tree.get(term) == null)
			tree.put(term, new LinkedHashSet<E>());

		tree.get(term).add(e);
	}

	@Override
	public void clear() {
		tree.clear();
	}

	@Override
	public Set<String> getAllTerms() {
		return Collections.unmodifiableSet(tree.keySet());
	}

	@Override
	public Set<E> getAllElements() {
		Set<E> result = new LinkedHashSet<E>();
		for (Map.Entry<String, Set<E>> e : tree.entrySet())
			result.addAll(e.getValue());

		return result;
	}

	@Override
	public Map<String, Set<E>> getAllTermsAndElements() {
		return Collections.unmodifiableMap(tree);
	}

	@Override
	public Set<E> remove(String term) {
		Set<E> elements = tree.remove(term);
		return elements != null ? elements : Collections.<E> emptySet();
	}

	@Override
	public boolean removeAll(Set<E> elements) {
		boolean result = false;
		for (Set<E> set : tree.values()) {
			if (set.removeAll(elements))
				result = true;
		}

		return result;
	}

	@Override
	public Set<E> lookup(String term) {
		Set<E> resultSet = new TreeSet<E>();
		String fromElement = term;

		char lastQueryChar = term.charAt(term.length() - 1);
		String nextChar = term.substring(0, term.length() - 1) + ((char) (lastQueryChar + 1));

		String toElement = nextChar;

		if (fromElement == null || toElement == null) {
			return resultSet;
		}

		SortedMap<String, Set<E>> partialView = tree.subMap(fromElement, toElement);
		for (Map.Entry<String, Set<E>> e : partialView.entrySet())
			resultSet.addAll(e.getValue());

		return resultSet;
	}

}
