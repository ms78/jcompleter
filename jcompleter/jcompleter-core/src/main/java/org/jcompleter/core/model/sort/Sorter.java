package org.jcompleter.core.model.sort;

import java.util.Comparator;

import org.jcompleter.core.model.Completer;

/**
 * An interface to sort the response results. The type and order of the sorting
 * operation is determined individually by each {@link Completer}
 * implementation.
 * 
 * @author Muthanna Alshoufi
 *
 * @param <E>
 *            the type of elements
 */
public interface Sorter<E> extends Comparator<E> {

}
