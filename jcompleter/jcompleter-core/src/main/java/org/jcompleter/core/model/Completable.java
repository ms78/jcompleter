package org.jcompleter.core.model;

import java.io.Serializable;
import java.util.Locale;

/**
 * This interface must be implemented by entities that are to be indexed for
 * auto-completion functionality by JCompleter.
 * 
 * @author Muthanna Alshoufi
 *
 */
public interface Completable extends Serializable {

	/**
	 * Returns a unique identifier of the completable entity.
	 * 
	 * @return the unique identifier
	 */
	String getCompletableId();

	/**
	 * Returns the String value provided by the completable entity.
	 * 
	 * @return the text value
	 */
	String getCompletableText();

	/**
	 * Gets the locale of this completable.
	 * 
	 * @return the locale of this completable
	 */
	Locale getCompletableLocale();

	/**
	 * Returns the initial score of this completable entity.
	 * 
	 * @return a double score
	 */
	Double getCompletableScore();

}
