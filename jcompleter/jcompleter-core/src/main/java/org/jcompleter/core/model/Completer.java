package org.jcompleter.core.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jcompleter.core.cache.Cache;
import org.jcompleter.core.cache.CacheFactory;
import org.jcompleter.core.cache.DefaultCacheFactory;
import org.jcompleter.core.exception.CompleterException;
import org.jcompleter.core.exception.CompleterInitializationException;
import org.jcompleter.core.exception.CompleterInvalidRequestException;
import org.jcompleter.core.model.api.request.AutocompleteRequest;
import org.jcompleter.core.model.api.request.Request;
import org.jcompleter.core.model.api.response.AutocompleteResponse;
import org.jcompleter.core.model.api.response.Response;
import org.jcompleter.core.model.index.DefaultIndexerFactory;
import org.jcompleter.core.model.index.Indexer;
import org.jcompleter.core.model.index.IndexerFactory;
import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderAdapter;
import org.jcompleter.core.model.load.LoaderFactory;
import org.jcompleter.core.model.normalize.DefaultNormalizerFactory;
import org.jcompleter.core.model.normalize.Normalizer;
import org.jcompleter.core.model.normalize.NormalizerFactory;
import org.jcompleter.core.model.sort.DefaultSorterFactory;
import org.jcompleter.core.model.sort.Sorter;
import org.jcompleter.core.model.sort.SorterFactory;
import org.jcompleter.core.model.sort.SorterType;
import org.jcompleter.core.model.tokenize.DefaultTokenizerFactory;
import org.jcompleter.core.model.tokenize.Tokenizer;
import org.jcompleter.core.model.tokenize.TokenizerFactory;

/**
 * Core JCompleter component. Each completer is aware of how to:
 * <ul>
 * <li>Load user data from a {@link Loader} obtained from a
 * {@link LoaderFactory} instance
 * <li>Normalize the data with a {@link Normalizer} obtained from a
 * {@link NormalizerFactory} instance
 * <li>Tokenize the data with a {@link Tokenizer} obtained from a
 * {@link TokenizerFactory} instance
 * <li>Index the normalized tokenized data with an {@link Indexer} obtained
 * from an {@link IndexerFactory} instance
 * <li>Cache frequent queries with a {@link Cache} obtained from a
 * {@link CacheFactory} instance
 * <li>Sort result elements with a {@link Sorter} obtained from a
 * {@link SorterFactory} instance
 * </ul>
 *
 * Example: Create a {@code Completer} and use it as follows:
 * 
 * <pre>
 * public class Country implements <strong>Completable</strong> {
 * 	private Integer id;
 * 	private String name;
 * 	private String currency;
 * 	public String <strong>getCompletableText()</strong> {
 * 		return name;
 * 	}
 * }
 * 
 * public class CountryLoader implements <strong>Loader</strong> {
 * 	public Collection&lt;? extends Completable&gt; <strong>getElements()</strong> {
 * 		return yourService.getCountries();
 * 	}
 * }
 * 
 * public class CountryLoaderFactory implements <strong>LoaderFactory</strong> {
 * 	public Loader <strong>create()</strong> {
 * 		return new CountryLoader();
 * 	}
 * }
 * 
 * Completer&lt;Element&gt; completer = new Completer&lt;Element&gt;()
 * .setName("countryCompleter")
 * .setLoaderFactory(<strong>new CountryLoaderFactory()</strong>)
 * .build();
 * 
 * AutocompleteRequest req = new AutocompleteRequest("a", Locale.ENGLISH);
 * AutocompleteResponse res = (AutocompleteResponse) completer.process(req);
 * for (Completable c : res.getCompletables())
 * 	System.out.println(c.getCompletableText());
 * </pre>
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class Completer<E extends Element> {

	private final Logger logger = Logger.getLogger(this.getClass().getName());

	private static final Integer DEFAULT_NUMBER_OF_RESULTS = 10;
	private static final Integer DEFAULT_NUMBER_OF_QUERIES_TO_CACHE = 500;
	private static final Integer DEFAULT_MINIMUM_QUERY_LENGTH_TO_CACHE = 1;
	private static final Integer DEFAULT_MAXIMUM_QUERY_LENGTH_TO_CACHE = 5;
	
	private static final Integer LOCK_WAIT_TIME_SECONDS = 3;

	private CacheFactory<String, Set<E>> cacheFactory;
	private SorterFactory<E> sorterFactory;
	private IndexerFactory<E> indexerFactory;
	private NormalizerFactory normalizerFactory;
	private TokenizerFactory tokenizerFactory;

	private String name;
	private LoaderAdapter<E> loaderAdapter;
	private Cache<String, Set<E>> cache;
	private Map<Locale, Tokenizer> tokenizerMap;
	private Map<Locale, Normalizer> normalizerMap;
	private Map<SorterType, Map<Locale, Sorter<E>>> sorterMap;
	private Map<Locale, Indexer<E>> indexerMap;

	private Integer numberOfResults;
	private Integer numberOfQueriesToCache;
	private Integer minimumQueryLengthToCache;
	private Integer maximumQueryLengthToCache;
	
	private Map<Class<? extends Request>, Command> commandMap;
	
	private Lock lock;

	public Completer() {

		lock = new ReentrantLock();
		
		// Initialize all needed factories
		this.cacheFactory = new DefaultCacheFactory<String, Set<E>>();
		this.sorterFactory = new DefaultSorterFactory<E>();
		this.indexerFactory = new DefaultIndexerFactory<E>();
		this.normalizerFactory = new DefaultNormalizerFactory();
		this.tokenizerFactory = new DefaultTokenizerFactory();

		this.normalizerMap = new HashMap<Locale, Normalizer>();
		this.indexerMap = new HashMap<Locale, Indexer<E>>();

		this.sorterMap = new HashMap<SorterType, Map<Locale, Sorter<E>>>();
		// Register the supported sorters for now
		this.sorterMap.put(SorterType.LEXICOGRAPHIC, new HashMap<Locale, Sorter<E>>());
		this.sorterMap.put(SorterType.SCORE, new HashMap<Locale, Sorter<E>>());

		this.tokenizerMap = new HashMap<Locale, Tokenizer>();

		this.numberOfResults = DEFAULT_NUMBER_OF_RESULTS;
		this.numberOfQueriesToCache = DEFAULT_NUMBER_OF_QUERIES_TO_CACHE;
		this.minimumQueryLengthToCache = DEFAULT_MINIMUM_QUERY_LENGTH_TO_CACHE;
		this.maximumQueryLengthToCache = DEFAULT_MAXIMUM_QUERY_LENGTH_TO_CACHE;
		
		// Init and register commands
		this.commandMap = new HashMap<Class<? extends Request>, Completer.Command>();
		this.commandMap.put(AutocompleteRequest.class, new AutocompleteCommand());
	}

	public String getName() {
		return name;
	}

	public Completer<E> setName(String name) {
		this.name = name;
		return this;
	}

	public Integer getNumberOfQueriesToCache() {
		return numberOfQueriesToCache;
	}

	public Completer<E> setNumberOfQueriesToCache(Integer numberOfQueriesToCache) {
		if (numberOfQueriesToCache != null)
			this.numberOfQueriesToCache = numberOfQueriesToCache;
		return this;
	}

	public Integer getMinimumQueryLengthToCache() {
		return minimumQueryLengthToCache;
	}

	public Completer<E> setMinimumQueryLengthToCache(Integer minimumQueryLengthToCache) {
		if (minimumQueryLengthToCache != null)
			this.minimumQueryLengthToCache = minimumQueryLengthToCache;
		return this;
	}

	public Integer getMaximumQueryLengthToCache() {
		return maximumQueryLengthToCache;
	}

	public Completer<E> setMaximumQueryLengthToCache(Integer maximumQueryLengthToCache) {
		if (maximumQueryLengthToCache != null)
			this.maximumQueryLengthToCache = maximumQueryLengthToCache;
		return this;
	}

	public Completer<E> setSorterFactory(SorterFactory<E> sorterFactory) {
		if (sorterFactory != null)
			this.sorterFactory = sorterFactory;
		return this;
	}

	public Completer<E> setCacheFactory(CacheFactory<String, Set<E>> cacheFactory) {
		if (cacheFactory != null)
			this.cacheFactory = cacheFactory;
		return this;
	}

	public Completer<E> setIndexerFactory(IndexerFactory<E> indexerFactory) {
		if (indexerFactory != null)
			this.indexerFactory = indexerFactory;
		return this;
	}

	public Completer<E> setNormalizerFactory(NormalizerFactory normalizerFactory) {
		if (normalizerFactory != null)
			this.normalizerFactory = normalizerFactory;
		return this;
	}

	public Completer<E> setTokenizerFactory(TokenizerFactory tokenizerFactory) {
		if (tokenizerFactory != null)
			this.tokenizerFactory = tokenizerFactory;
		return this;
	}

	public Completer<E> setLoaderFactory(LoaderFactory loaderFactory) {
		if (loaderFactory != null) {
			this.loaderAdapter = new LoaderAdapter<E>(loaderFactory.create());
		}

		return this;
	}

	public Completer<E> build() throws CompleterInitializationException {

		if (name == null)
			throw new CompleterInitializationException("Completer name is missing. Can not proceed.");
		if (loaderAdapter == null)
			throw new CompleterInitializationException("Completer loader is missing. Can not proceed.");

		try {
			if (lock.tryLock(LOCK_WAIT_TIME_SECONDS, TimeUnit.SECONDS)) {
				clearElements();
				Map<Locale, Collection<E>> elements = loaderAdapter.getElements();
				for (Locale locale : elements.keySet()) {
					if (indexerMap.get(locale) == null) {
						Indexer<E> index = indexerFactory.create();
						indexerMap.put(locale, index);
					}

					if (normalizerMap.get(locale) == null) {
						Normalizer normalizer = normalizerFactory.create(locale);
						normalizerMap.put(locale, normalizer);
					}

					if (tokenizerMap.get(locale) == null) {
						Tokenizer tokenizer = tokenizerFactory.create(locale);
						tokenizerMap.put(locale, tokenizer);
					}

					if (sorterMap.get(SorterType.LEXICOGRAPHIC).get(locale) == null) {
						Sorter<E> lexicographicSorter = sorterFactory.create(locale, SorterType.LEXICOGRAPHIC);
						sorterMap.get(SorterType.LEXICOGRAPHIC).put(locale, lexicographicSorter);
					}

					if (sorterMap.get(SorterType.SCORE).get(locale) == null) {
						Sorter<E> scoreSorter = sorterFactory.create(locale, SorterType.SCORE);
						sorterMap.get(SorterType.SCORE).put(locale, scoreSorter);
					}

				}

				for (Entry<Locale, Collection<E>> entry : elements.entrySet()) {
					Locale locale = entry.getKey();
					Normalizer normalizer = normalizerMap.get(locale);
					Tokenizer tokenizer = tokenizerMap.get(locale);
					Indexer<E> index = indexerMap.get(locale);
					for (E e : entry.getValue()) {
						String normalizedInput = normalizer.normalize(e.getCompletableText());
						Map<String, List<String>> tokens = tokenizer.tokenize(normalizedInput);
						for (Map.Entry<String, List<String>> token : tokens.entrySet()) {
							for (String term : token.getValue())
								index.add(term, e);
						}
					}
				}

				if (cache == null) {
					cache = cacheFactory.create(numberOfQueriesToCache);
				} else {
					flushCache();
				}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Building completer elements failed", e);
			throw new CompleterInitializationException(e);
		} finally {
			lock.unlock();
		}
		
		return this;
	}

	public void destroy() {
		try {
			if (lock.tryLock(LOCK_WAIT_TIME_SECONDS, TimeUnit.SECONDS)) {
				clearElements();
				flushCache();
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Destroying completer failed", e);
		} finally {
			lock.unlock();
		}
		
	}

	private void clearElements() {
		indexerMap.clear();
	}
	
	public void flushCache() {
		if (cache != null)
			cache.flush();
	}

	public Response process(final Request request) throws CompleterException {
		// Make sure we got a valid servable request
		if (request == null || !request.isValid()) {
			throw new CompleterInvalidRequestException("The following request is invalid: " + request);
		}
		
		Command command = null;
		for(Map.Entry<Class<? extends Request>, Command> entry : commandMap.entrySet())
			if(entry.getKey().isAssignableFrom(request.getClass()))
				command = entry.getValue();
		
		if(command == null)
			throw new CompleterInvalidRequestException("The following request is not recognized: " + request);
		
		return command.execute(request);
	}
	
	private boolean considerCaching(String query) {
		return cache != null && numberOfQueriesToCache > 0 && query.length() >= minimumQueryLengthToCache
				&& query.length() <= maximumQueryLengthToCache;
	}
	
	private interface Command {
		Response execute(Request request) throws CompleterException;
	}
	
	private class AutocompleteCommand implements Command {

		@Override
		public AutocompleteResponse execute(Request request) throws CompleterException {
			AutocompleteRequest autocompleteRequest = (AutocompleteRequest) request;
			String query = autocompleteRequest.getQuery();
			Locale locale = autocompleteRequest.getLocale();
			Normalizer normalizer = normalizerMap.get(locale);
			String normalizedQuery = normalizer != null ? normalizer.normalize(query) : query;

			// Check cache
			boolean considerCaching = considerCaching(normalizedQuery);
			String cacheKey = null;
			Set<E> elements = null;
			if (considerCaching) {
				cacheKey = normalizedQuery + "_" + locale;
				elements = cache.get(cacheKey);
			}

			if (elements == null) {
				elements = new LinkedHashSet<E>();
				Indexer<E> index = indexerMap.get(locale);
				if (index != null) {
					try {
						Tokenizer tokenizer = tokenizerMap.get(locale);
						Map<String, List<String>> tokens = tokenizer.tokenize(normalizedQuery);
						for (Map.Entry<String, List<String>> token : tokens.entrySet()) {
							for (String term : token.getValue()) {
								Set<E> set = index.lookup(term);
								if (elements.isEmpty())
									elements.addAll(set);
								else
									elements.retainAll(set);
							}
						}
	
						if (considerCaching && !elements.isEmpty()) {
							cache.put(cacheKey, elements);
						}
					
					} catch (Exception e) {
						throw new CompleterException("Something went wrong while trying to serve request: " + request, e);
					}
				}
			}
			
			List<E> list = new ArrayList<E>(elements);
			SorterType sortType = autocompleteRequest.getSorterType();
			Sorter<E> sorter = sorterMap.get(sortType) != null ? sorterMap.get(sortType).get(locale) : null;
			if (sorter != null)
				list.sort(sorter);

			Integer requestNumberOfResults = autocompleteRequest.getNumberOfResults();
			int finalNumberOfResults = (requestNumberOfResults != null && requestNumberOfResults > 0)
					? requestNumberOfResults : numberOfResults;

			// Do we need to limit the returned result?
			if (list.size() > finalNumberOfResults)
				list = list.subList(0, finalNumberOfResults);

			return new AutocompleteResponse(request, list);
		}

	}

}
