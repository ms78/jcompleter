package org.jcompleter.core.model.normalize;

import java.util.Locale;

/**
 * Creates {@link Normalizer} instances.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface NormalizerFactory {

	/**
	 * Creates {@link Normalizer} implementations.
	 * 
	 * @param locale
	 *            the Locale to use when creating the normalizer
	 * @return a {@link Normalizer} implementaion instance
	 */
	Normalizer create(Locale locale);

}
