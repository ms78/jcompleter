package org.jcompleter.external.spring.load;

import org.jcompleter.core.external.JCompleterContextBridge;
import org.jcompleter.core.model.load.Loader;
import org.jcompleter.core.model.load.LoaderFactory;
import org.jcompleter.external.spring.JCompleterSpringContextBridge;

/**
 * All loader factories in a Spring application have to extend this abstract
 * factory class and implement
 * {@link JCompleterSpringLoaderFactory#getLoaderBeanClass()}. If you want more
 * control for whatever reason, you can create your own factory and implement
 * {@link LoaderFactory} directly instead.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public abstract class JCompleterSpringLoaderFactory implements LoaderFactory {

	@Override
	public Loader create() {
		JCompleterContextBridge bridge = JCompleterSpringContextBridge.get();

		if (bridge != null) {
			Object loader = bridge.getBean(getLoaderBeanClass());
			if(loader instanceof Loader)
				return (Loader) loader;
		}

		return null;
	}

	public abstract Class<?> getLoaderBeanClass();

}
