package org.jcompleter.external.spring;

import org.jcompleter.core.external.JCompleterContextBridge;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Provides JCompleter with Spring context and thus allows loading loader beans
 * and read the data elements.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class JCompleterSpringContextBridge implements JCompleterContextBridge, ApplicationContextAware {

	private static ApplicationContext APP_CONTEXT;

	public static JCompleterContextBridge get() {
		return APP_CONTEXT.getBean(JCompleterContextBridge.class);
	}

	@Override
	public <T> T getBean(Class<T> beanClass) {
		return APP_CONTEXT.getBean(beanClass);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		APP_CONTEXT = applicationContext;
	}

}
