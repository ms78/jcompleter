package org.jcompleter.web.convert;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Item wrapper.
 * 
 * @author Muthanna Jan 2, 2017
 */
public class Items<E> extends LinkedList<E> {

	private static final long serialVersionUID = 1L;

	public Items() {
	}

	public Items(Collection<? extends E> c) {
		super(c);
	}

}
