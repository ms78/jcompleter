package org.jcompleter.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jcompleter.core.CompleterFacade;
import org.jcompleter.core.exception.CompleterException;
import org.jcompleter.core.model.api.response.AutocompleteResponse;
import org.jcompleter.core.model.sort.SorterType;
import org.jcompleter.core.util.CommonUtil;
import org.jcompleter.web.convert.Converter;
import org.jcompleter.web.convert.JsonConverter;
import org.jcompleter.web.convert.XmlConverter;

/**
 * Main JCompleter Servlet acting as the main entry point for web requests.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class CompleterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private final Logger logger = Logger.getLogger(CompleterServlet.class.getName());

	private final static String DEFAULT_RESPONSE_ENCODING = "UTF-8";
	private final static String DEFAULT_CACHE_CONTROL = "no-cache";

	private final static String CONTENT_TYPE_TEXT_HTML = "text/html";
	private final static String CONTENT_TYPE_JSON = "application/json";
	private final static String CONTENT_TYPE_XML = "application/xml";
	private final static String CONTENT_TYPE_JAVASCRIPT = "application/javascript";

	private final static String RESPONSE_JSON = "json";
	private final static String RESPONSE_XML = "xml";

	private final static String INIT_PARAM_RESPONSE_ENCODING = "responseEncoding";
	private final static String INIT_PARAM_CACHE_CONTROL = "Cache-Control";

	private final static String REQUEST_PARAM_QUERY = "query";
	private final static String REQUEST_PARAM_COMPLETER_NAME = "completerName";
	private final static String REQUEST_PARAM_NUMBER_OF_RESULTS = "numberOfResults";
	private final static String REQUEST_PARAM_LANGUAGE = "language";
	private final static String REQUEST_PARAM_RESPONSE_TYPE = "responseType";
	private final static String REQUEST_PARAM_SORTER_TYPE = "sorterType";
	private final static String REQUEST_PARAM_CALLBACK = "callback";

	private String responseEncoding;
	private String cacheControl;
	private boolean serviceAvailable;

	protected CompleterFacade completerFacade;
	private Map<String, Converter> converters;

	private String getInitParam(String paramName, String defaultValue) {
		String paramValue = getInitParameter(paramName);
		return (!CommonUtil.isBlank(paramValue)) ? paramValue : defaultValue;
	}

	@Override
	public void init() throws ServletException {
		super.init();

		// Set configured response encoding
		responseEncoding = getInitParam(INIT_PARAM_RESPONSE_ENCODING, DEFAULT_RESPONSE_ENCODING);

		// Set configured cache control
		cacheControl = getInitParam(INIT_PARAM_CACHE_CONTROL, DEFAULT_CACHE_CONTROL);

		// Register the response converters and initialize JCompleter facade
		try {
			converters = new HashMap<String, Converter>();
			converters.put(RESPONSE_JSON, new JsonConverter());
			converters.put(RESPONSE_XML, new XmlConverter());

			initCompleterFacade();

			serviceAvailable = true;
		} catch (CompleterException e) {
			logger.log(Level.SEVERE, "JCompleterServlet failed to initialize the facade. Service is unavailable.", e);
			serviceAvailable = false;
		}

	}

	protected void initCompleterFacade() throws CompleterException {
		// Init facade
		completerFacade = new CompleterFacade();
	}

	@Override
	public void destroy() {
		super.destroy();
		if (completerFacade != null) {
			completerFacade.close();
		}
	}

	@Override
	protected void doPost(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
			throws ServletException, IOException {
		doGet(httpRequest, httpResponse);
	}

	@Override
	protected void doGet(HttpServletRequest httpRequest, HttpServletResponse httpResponse)
			throws ServletException, IOException {
		AutocompleteResponse completerResponse = null;
		try {
			if (serviceAvailable) {
				String completerName = httpRequest.getParameter(REQUEST_PARAM_COMPLETER_NAME);
				String query = httpRequest.getParameter(REQUEST_PARAM_QUERY);
				if (!CommonUtil.isBlank(completerName) && !CommonUtil.isBlank(query)) {
					String numberOfResults = httpRequest.getParameter(REQUEST_PARAM_NUMBER_OF_RESULTS);
					Integer numberOfResultsInt = null;
					if (!CommonUtil.isBlank(numberOfResults)) {
						try {
							numberOfResultsInt = Integer.parseInt(numberOfResults);
						} catch (NumberFormatException nfe) {
							logger.severe(
									numberOfResults + "is not aa number in param: " + REQUEST_PARAM_NUMBER_OF_RESULTS);
						}
					}

					String language = httpRequest.getParameter(REQUEST_PARAM_LANGUAGE);
					Locale locale = CommonUtil.parseLocale(language);
					if (locale != null && !CommonUtil.isValid(locale)) {
						locale = null;
					}

					String sorterType = httpRequest.getParameter(REQUEST_PARAM_SORTER_TYPE);
					completerResponse = completerFacade.autocomplete(completerName, query, locale, numberOfResultsInt,
							SorterType.fromValue(sorterType));
				}
			}
		} catch (CompleterException ce) {
			logger.severe(ce.getCause() != null ? ce.getCause().getMessage() : ce.getMessage());
		} finally {
			sendResponse(httpRequest, httpResponse, completerResponse);
		}
	}

	/**
	 * Sends the response to the caller.
	 * 
	 * @param httpRequest
	 *            the HTTP request sent by the caller
	 * @param httpResponse
	 *            the HTTP response returned to the caller
	 * @param completerResponse
	 *            the completer response
	 */
	protected void sendResponse(HttpServletRequest httpRequest, HttpServletResponse httpResponse,
			AutocompleteResponse completerResponse) {

		// Set headers
		httpResponse.setContentType(CONTENT_TYPE_TEXT_HTML);
		httpResponse.setCharacterEncoding(responseEncoding);
		httpResponse.setHeader(INIT_PARAM_CACHE_CONTROL, cacheControl);

		PrintWriter out = null;
		try {
			out = httpResponse.getWriter();
			if (completerResponse != null) {

				httpResponse.setStatus(HttpServletResponse.SC_OK);
				String responseTypeParam = httpRequest.getParameter(REQUEST_PARAM_RESPONSE_TYPE);
				Converter converter = getConverter(responseTypeParam);
				String callbackParam = httpRequest.getParameter(REQUEST_PARAM_CALLBACK);
				if (callbackParam != null && !callbackParam.isEmpty()) {
					out.println(callbackParam + "(" + converter.convert(completerResponse) + ");");
					httpResponse.setContentType(CONTENT_TYPE_JAVASCRIPT);
				} else {
					out.println(converter.convert(completerResponse));
					String contentType = converter instanceof XmlConverter ? CONTENT_TYPE_XML : CONTENT_TYPE_JSON;
					httpResponse.setContentType(contentType);
				}
			} else {
				httpResponse.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
		} catch (IOException e) {
			logger.severe(e.getMessage());
		} finally {
			if (out != null) {
				out.flush();
				out.close();
			}
		}
	}

	public Converter getConverter(String responseType) {
		String key = RESPONSE_JSON;
		if (responseType != null && responseType.trim().equalsIgnoreCase(RESPONSE_XML))
			key = RESPONSE_XML;

		return converters.get(key);
	}

	/**
	 * Get information about this Servlet.
	 * 
	 */
	@Override
	public String getServletInfo() {
		return "JCompleter Servlet";
	}

}
