package org.jcompleter.web.convert;

import java.util.Collections;

import org.jcompleter.core.model.api.response.AutocompleteResponse;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Simple JSON implementation of {@link Converter}
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class JsonConverter implements Converter {

	private static final ObjectMapper JSON_MAPPER = new ObjectMapper();

	static {
		JSON_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		JSON_MAPPER.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
		JSON_MAPPER.disable(MapperFeature.CAN_OVERRIDE_ACCESS_MODIFIERS);
	}

	public static <T> String serializeToJson(T obj) {
		try {
			return JSON_MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
		}

		return null;
	}

	@Override
	public String convert(AutocompleteResponse response) {

		if (response == null || response.getElements() == null) {
			return serializeToJson(Collections.emptyList());
		}

		return serializeToJson(response.getCompletables());
	}

}
