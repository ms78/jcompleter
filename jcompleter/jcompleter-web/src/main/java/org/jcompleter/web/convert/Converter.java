package org.jcompleter.web.convert;

import org.jcompleter.core.model.api.response.AutocompleteResponse;

/**
 * Response converter whose implementations should generate the final data
 * format to be sent back to the caller.
 * 
 * @author Muthanna Alshoufi
 * 
 */
public interface Converter {

	/**
	 * Converts the elements of the passed in response into another format such
	 * as JSON and XML.
	 * 
	 * 
	 * @param response
	 *            the response to convert
	 * @return the converted completer response
	 */
	String convert(AutocompleteResponse response);

}
