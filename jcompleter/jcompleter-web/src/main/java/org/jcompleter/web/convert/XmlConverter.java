package org.jcompleter.web.convert;

import org.jcompleter.core.model.Completable;
import org.jcompleter.core.model.api.response.AutocompleteResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

/**
 * Simple XML implementation of {@link Converter}
 * 
 * @author Muthanna Alshoufi
 * 
 */
public class XmlConverter implements Converter {

	private static final XmlMapper XML_MAPPER = new XmlMapper();

	static {
		// XML_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
		XML_MAPPER.enable(SerializationFeature.WRAP_ROOT_VALUE);
		XML_MAPPER.registerModule(new JaxbAnnotationModule());
	}

	public static <T> String serializeToXml(T obj) {
		try {
			return XML_MAPPER.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
		}

		return null;
	}

	@Override
	public String convert(AutocompleteResponse response) {

		if (response == null || response.getElements() == null || response.getElements().isEmpty()) {
		}

		return serializeToXml(new Items<Completable>(response.getCompletables()));
	}

}
